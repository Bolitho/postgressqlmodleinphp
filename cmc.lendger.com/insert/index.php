<?php
require_once('../database/DatabaseInsertOperation.php');

class InsertionOperation extends DatabaseInsertOperations
{
    private $isInsertMany;

    public function __construct()
    {
        header("Content-Type: application/json");

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            http_response_code(405);
            echo json_encode(["error" => "Only POST requests are allowed here"]);
            exit();
        }

        $data = json_decode(file_get_contents('php://input'), true);
        $this->isInsertMany = isset($data['isManyInsert']) ? $data['isManyInsert'] : false;

        $settings = [
            "tableName" => $data['table'],
            "data" => $data,
            "isManyRecords" => $this->isInsertMany
        ];

        parent::__construct($settings);

        try {
            $results = $this->isInsertMany ? $this->insertMany() : $this->insertOne();
            echo json_encode($results);
        } catch (\Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }
}

new InsertionOperation();
?>
