-- Drop the custom aggregate if it exists
DROP AGGREGATE IF EXISTS subtract_agg(numeric, numeric);

-- Drop the custom final function if it exists
DROP FUNCTION IF EXISTS custom_sum_agg_finalfn(custom_sum_agg_state);

-- Drop the custom transition function if it exists
DROP FUNCTION IF EXISTS custom_sum_agg_transfn(custom_sum_agg_state, numeric, numeric);

-- Drop the custom state type if it exists and its dependents
DROP TYPE IF EXISTS custom_sum_agg_state CASCADE;

-- Create the custom aggregate state type to hold the intermediate sum values
CREATE TYPE custom_sum_agg_state AS (
  sum_values numeric[],
  value_to_subtract numeric
);

-- Create the state transition function to calculate the sum and store other values
CREATE OR REPLACE FUNCTION custom_sum_agg_transfn(
  state custom_sum_agg_state,
  value numeric,
  value_to_subtract numeric
) RETURNS custom_sum_agg_state AS $$
BEGIN
  state.sum_values := COALESCE(state.sum_values, ARRAY[]::numeric[]) || value;
  state.value_to_subtract := value_to_subtract;
  RETURN state;
END;
$$ LANGUAGE plpgsql;

-- Create the final function to return the result (difference between sum and value_to_subtract)
CREATE OR REPLACE FUNCTION custom_sum_agg_finalfn(state custom_sum_agg_state)
RETURNS numeric AS $$
BEGIN
    IF array_length(state.sum_values, 1) IS NOT NULL THEN
        RETURN (SELECT COALESCE(SUM(value), 0) FROM unnest(state.sum_values) AS value) - state.value_to_subtract;
    ELSE
        RETURN NULL;
    END IF;
END;
$$ LANGUAGE plpgsql;

-- Create the aggregate using the state type and functions
CREATE AGGREGATE subtract_agg (numeric, numeric) (
  SFUNC = custom_sum_agg_transfn,
  STYPE = custom_sum_agg_state,
  FINALFUNC = custom_sum_agg_finalfn,
  INITCOND = '("{}", 0)' -- Use empty array and 0 as the initial state
);
