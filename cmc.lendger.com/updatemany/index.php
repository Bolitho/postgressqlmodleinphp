 <?php require_once '../database/DatabaseUpdater.php';

class UpdateMany extends DatabaseUpdater
{
    public function __construct()
    {
      header("Content-Type: application/json");
        if (!isset($_GET['table'])) {
            http_response_code(400); // Bad Request
            echo json_encode(["error" => "Table name is required"]);
            exit();
        }

        $table = trim($_GET['table']);
        parent::__construct($table);

        // Check if the request method is PUT
        if ($_SERVER['REQUEST_METHOD'] !== 'PUT') {
            http_response_code(405); // Method Not Allowed
            echo json_encode(['error' => 'Only PUT requests are allowed']);
            exit();
        }

        $data = json_decode(file_get_contents('php://input'), true);

        if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
            http_response_code(400); // Bad Request
            echo json_encode(["error" => "Invalid JSON format"]);
            exit();
        }

        if (!isset($data['conditions'])) {
            http_response_code(400); // Bad Request
            echo json_encode(["error" => "Conditions are required"]);
            exit();
        }

        $conditions = [];
        foreach ($data['conditions'] as $key => $value) {
            $conditions[] = [$key => $value];
        }
        unset($data['conditions']);

        echo "Conditions: ";
        print_r($conditions);
        echo "Table Name: " . $table;

        try {
            $this->updateMany($data, $conditions);
        } catch (\Exception $e) {
            throw new \Exception("Error Processing Request: " . $e->getMessage());
        }
    }
}

new UpdateMany();
 ?>
