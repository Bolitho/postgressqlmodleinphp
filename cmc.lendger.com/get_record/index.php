<?php require_once('../database/DatabaseFetcher.php');

class FindOne extends DatabaseFetcher {

    private $table;
    private $id;

    public function __construct() {
        $this->table = trim($_GET['table']) ?? null;
        $this->id = trim($_GET['id']) ?? null;
        parent::__construct($this->table);

        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            http_response_code(405); // Method Not Allowed
            echo json_encode(['error' => 'Only GET requests are allowed']);
            exit();
        }

        $condition = [['id' => $this->id]];

        try {
            $response = $this->findInfoOne($condition);
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(["error" => $e->getMessage()]);
        }
    }
}

new FindOne();
 ?>
