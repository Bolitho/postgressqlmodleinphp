<?php require_once('../database/DatabaseDeleteOperation.php');

class Delete extends DatabaseDeleteOperation
{
    private $isDeleteMany = false;
    private $isDeleteOne = false;
    private $isDeleteAll = false;

    function __construct()
    {
        header("Content-Type: application/json");

        // Check if the request method is DELETE
        if ($_SERVER['REQUEST_METHOD'] !== "DELETE") {
            http_response_code(405); // Method Not Allowed
            echo json_encode(['error' => 'Only Delete requests are allowed']);
            exit();
        }

        // Extract data from the URL parameters (using $_GET)
        if (isset($_GET['isDeleteAll'])) {
            $this->isDeleteAll = $_GET['isDeleteAll'];
        }
        if (isset($_GET['isDeleteOne'])) {
            $this->isDeleteOne = $_GET['isDeleteOne'];
        }
        if (isset($_GET['isDeleteMany'])) {
            $this->isDeleteMany = $_GET['isDeleteMany'];
        }

        // Make sure a valid 'tableName' key exists in the request data
        if (empty($_GET['tableName'])) {
            http_response_code(400); // Bad Request
            echo json_encode(['error' => 'Table name is missing']);
            exit();
        }
         $isDeleteManyDifferentRows = $_GET["isDeleteManyDifferentRows"]??false;
         $conditions = isset($_GET['conditions']) ? $_GET['conditions'] : '';
         $urldecode = urldecode($conditions);
          $conditionsArray = json_decode($urldecode, true); // Add true as the second parameter
          $isDeleteFromParent = $_GET["isDeleteFromParent"]??false;


        // Call the parent constructor with the correct settings array
        $settings = ["tableName" => $_GET["tableName"], "conditions" =>$conditionsArray, 'isDeleteManyDifferentRows'=>$isDeleteManyDifferentRows, "isDeleteFromParent"=>$isDeleteFromParent];
        parent::__construct($settings);
       $response;
        try {
            if ($this->isDeleteAll) {
                $response=$this->deleteAll();
            }
            if ($this->isDeleteOne) {
                $response=$this->deleteOne();
            }
            if ($this->isDeleteMany) {
                $response=$this->deleteMany();
            }

            echo json_encode(['response' =>$response]);

        } catch (Exception $e) {
            http_response_code(500); // Internal Server Error
            echo json_encode(['error' => $e->getMessage()]);
        }
    }
}

new Delete();
 ?>
