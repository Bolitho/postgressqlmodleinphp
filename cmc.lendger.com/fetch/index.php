<?php require_once('../database/DatabaseFetcher.php');

class Get extends DatabaseFetcher
{
    private $isAll;

    public function __construct()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $table = $data['table'] ?? null;
        if (isset($data["isAll"])) {
            $this->isAll = $data["isAll"];
        }else {
           $this->isAll = false;
        }

        echo json_encode(["table" => $table, "isManyToMany" => $data["isManyToMany"]]);
        echo "\n";
        parent::__construct($table, $data);

        header("Content-Type: application/json");

        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            http_response_code(405); // Method Not Allowed
            echo json_encode(['error' => 'Only GET requests are allowed']);
            exit();
        }

        try {
            //$tableNames = $this->fetchAll();
            if ($this->isAll) {
                $fetchedData = $this->findAll();
            } else {
                $fetchedData = $this->findInfoOne();
            }

            $response = $fetchedData;
            $response= json_encode(["response" => $response]);
            echo $response;
            return $response;

        } catch (Exception $e) {
            http_response_code(500); // Internal Server Error
            echo json_encode(['error' => $e->getMessage()]);
            // Log the error or handle it in an appropriate way
        }
    }
}

new Get();
 ?>
