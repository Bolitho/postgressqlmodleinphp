<?php

const OPERATORS = [
    'eq' => '=',
    'neq' => '!=',
    'gt' => '>',
    'lt' => '<',
    'gte' => '>=',
    'lte' => '<=',
    'like' => 'LIKE',
    'ilike' => 'ILIKE',
    'in' => 'IN',
    'not_in' => 'NOT IN',
    'between' => 'BETWEEN',
    'is_null' => 'IS NULL',
    'is_not_null' => 'IS NOT NULL',
    'any' => 'ANY',
    'all' => 'ALL',
    'exists' => 'EXISTS',
    'not_exists' => 'NOT EXISTS',
    'contains' => '@>',
    'contained_by' => '@<',
    // Add more operators as needed
];
?>
