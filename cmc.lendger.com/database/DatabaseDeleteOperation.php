<?php
require_once 'PostgresDataConnection.php';
require_once 'operators.php';


class DatabaseDeleteOperation extends PostgresDataConnection
{
    private $tableName;
    private $conditions;
    private $isManyToMany;
    private $isDeleteFromParent = false;
    private $condition = "";
    private $isDeleteManyDifferentRows;
    private $ConditionsValues = [];
    private $isConditionSet = false;
    private $connection;


    public function __construct($settings)
    {
         $this->connection = $this->getInstance()->getConnection();

        if (!isset($settings['tableName'])) {
            $this->handleTableNameError();
        }
        if (isset($settings['conditions'])) {
            $this->conditions = $settings['conditions'];
            $this->isConditionSet = true;
        }
        if (isset($settings['isDeleteFromParent'])) {
            $this->isDeleteFromParent = $settings['isDeleteFromParent'];
        }
        $this->isDeleteManyDifferentRows =$settings['isDeleteManyDifferentRows']??false;

        if ($this->isConditionSet && !$this->isDeleteManyDifferentRows) {
            $processedConditions = $this->processedConditions($this->conditions);
            $this->condition = $processedConditions['condition'];
            $this->ConditionsValues = $processedConditions['params'];
        }

        $this->tableName = $settings["tableName"];
        $table_meta_data = $this->getOneTable($this->tableName);
        echo json_encode(["table_meta_data"=>$table_meta_data]);

        $this->isManyToMany = strpos($table_meta_data["constraint_name"], 'manytomany') !== false;
         echo json_encode(["isDeleteFromParent"=>$this->isDeleteFromParent]);
        if ($this->isManyToMany && !$this->isDeleteFromParent) {


            $this->tableName = $table_meta_data['child_table'];
        }
    }

    private function handleTableNameError()
    {
        throw new \Exception("The table name is missing");
    }

    public function deleteOne()
    {
        $whereClause = " WHERE " . $this->condition;
        $column = null;

        if (!empty($this->conditions)) {
          $firstColumn = $this->conditions[0]['column'];

        }

        $sql = "DELETE FROM $this->tableName WHERE $firstColumn  IN (SELECT $firstColumn  FROM $this->tableName $whereClause LIMIT 1)";



        try {
            $statement = $this->connection->prepare($sql);
            $statement->execute($this->ConditionsValues);

            $rowCount = $statement->rowCount();
                return $rowCount > 0;
        } catch (PDOException $e) {
            http_response_code(400);
             throw new \Exception($e->getMessage());

        }
    }
    public function deleteMany()
  {
      if ($this->isDeleteManyDifferentRows) {
        echo json_encode(["conditions"=>$this->conditions]);

          foreach ($this->conditions as $value) {
              $processedConditions = $this->processedConditions($value);
              $this->condition = $processedConditions['condition'];
              $this->ConditionsValues = $processedConditions['params'];
              $this->delete();
          }
      } else {

          $this->delete();
      }
  }

  private function delete()
  {
      $whereClause = " WHERE " . $this->condition;
      $sql = "DELETE FROM $this->tableName $whereClause";

      try {
          $statement = $this->connection->prepare($sql);
          $statement->execute($this->ConditionsValues);

          $rowCount = $statement->rowCount();
          return $rowCount > 0;
      } catch (PDOException $e) {
          http_response_code(400);
          throw new Exception(json_encode(["error_delete" => $e->getMessage()]));
      }
  }


    public function deleteAll()
    {
        try {
            $sql = "DELETE FROM $this->tableName";
            $statement = $this->connection->prepare($sql);
            $statement->execute();
        } catch (PDOException $e) {
            throw new Exception("Error executing delete query: " . $e->getMessage());
        }
    }
}
?>
