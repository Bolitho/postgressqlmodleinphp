<?php
require_once 'PostgresDataConnection.php';
require_once 'operators.php';

class DeleteTables extends PostgresDataConnection
{
    private $tablesToDelete;
    private $pdo;
    private $database;
    function __construct($settings)
    {

        $this->database = $this->getInstance();
        $this->pdo = $this->database->getConnection();
        $this->tablesToDelete = $settings["tablesToDelete"] ?? [];
    }
    public function dropTables($index = 0)
 {


     if (!empty($this->tablesToDelete) && $index < count($this->tablesToDelete)) {
         $value = $this->tablesToDelete[$index];
         $sql = "DROP TABLE IF EXISTS $value CASCADE"; // Add "CASCADE" to drop dependent child tables as well



         try {
             $statement = $this->pdo->prepare($sql);
             echo json_encode(["statement" => $statement]);
             echo "\n";
             $statement->execute();



         } catch (PDOException $e) {
             throw new \Exception($e->getMessage());
         }

         // Recur to drop the next table
         $this->dropTables($index + 1);
     }
 }

}
?>
