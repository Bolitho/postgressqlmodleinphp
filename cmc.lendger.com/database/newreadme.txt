inserting multiple Records for manyto many json format
{
  "relationship": "manyToMany",
  "parentTableOne": "students",
  "parentTableTwo": "courses",
  "isManyRecords": true,
  "meta_data": [
    {
      "parentTableOne_pk": "23F2BF",
      "parentTableTwo_pk": "14C635"
    },
    {
      "parentTableOne_pk": "01DE70",
      "parentTableTwo_pk": "2C557E"
    }
  ]
}
Sure, here's the PHP version using associative arrays:

```php
<?php

$data = array(
    "relationship" => "manyToMany",
    "parentTableOne" => "students",
    "parentTableTwo" => "courses",
    "isManyRecords" => true,
    "meta_data" => array(
        array(
            "parentTableOne_pk" => "23F2BF",
            "parentTableTwo_pk" => "14C635"
        ),
        array(
            "parentTableOne_pk" => "01DE70",
            "parentTableTwo_pk" => "2C557E"
        )
    )
);

$jsonArray = array();
$jsonArray["relationship"] = $data["relationship"];
$jsonArray["parentTableOne"] = $data["parentTableOne"];
$jsonArray["parentTableTwo"] = $data["parentTableTwo"];
$jsonArray["isManyRecords"] = $data["isManyRecords"];

$jsonArray["meta_data"] = array();
foreach ($data["meta_data"] as $meta) {
    $metaArray = array();
    $metaArray["parentTableOne_pk"] = $meta["parentTableOne_pk"];
    $metaArray["parentTableTwo_pk"] = $meta["parentTableTwo_pk"];
    $jsonArray["meta_data"][] = $metaArray;
}

$jsonString = json_encode($jsonArray, JSON_PRETTY_PRINT);
echo $jsonString;
```

The output of this PHP code will be the same as the previous example:

```json
{
    "relationship": "manyToMany",
    "parentTableOne": "students",
    "parentTableTwo": "courses",
    "isManyRecords": true,
    "meta_data": [
        {
            "parentTableOne_pk": "23F2BF",
            "parentTableTwo_pk": "14C635"
        },
        {
            "parentTableOne_pk": "01DE70",
            "parentTableTwo_pk": "2C557E"
        }
    ]
}
```

In this version, we create an associative array `$jsonArray` and manually populate it with the key-value pairs from the original `$data` array. The `foreach` loop is used to convert the "meta_data" sub-array into the desired format within the `$jsonArray`. Then, we use `json_encode()` to convert the `$jsonArray` into a JSON string and print it.
for oneToMany of  different rows of dirrect values {
  "relationship": "oneToMany",
  "parentTable": "workers",
  "childTable": "amount",
  "isManyRecords": "true",
  "isManyInsert": "true",
  "meta_data": [
    [
      {
        "parentTable_pk": "CB97D1",
        "childTable_data": [
          {
            "column": "amount_paid",
            "value": 200,
            "constraint": "not null"
          }
        ]
      }
    ],
    [
      {
        "parentTable_pk": "1C5B23",
        "childTable_data": [
          {
            "column": "amount_paid",
            "value": 300,
            "constraint": "not null"
          }
        ]
      }
    ],
    [
      {
        "parentTable_pk": "2FE098",
        "childTable_data": [
          {
            "column": "amount_paid",
            "value": 600,
            "constraint": "not null"
          }
        ]
      }
    ]
  ]
}insertinf many records in onetone table {
  "relationship": "oneTOne",
  "parentTable": "workers",
  "childTable": "location",
  "isManyRecords": "true",
  "isManyInsert": "true",
  "meta_data": [
    [
      {
        "parentTable_pk": "CB97D1",
        "childTable_data": [
          {
            "column": "country",
            "value": "uganda",
            "constraint": "not null"
          },   {
            "column": "city",
            "value": "kampala",
            "constraint": "not null"
          },
            {
            "column": "address",
            "value": "bweya",
            "constraint": "null"
          },
          {
            "column": "longitude",
            "value": -446228,
            "constraint": "null"
          }
          ,{
            "column": "latitude",
            "value": 4462289,
            "constraint": "null"
          }
        ]
      }
    ],
      [
      {
        "parentTable_pk": "28F33A",
        "childTable_data": [
          {
            "column": "country",
            "value": "us",
            "constraint": "not null"
          },   {
            "column": "city",
            "value": "new york",
            "constraint": "not null"
          },
            {
            "column": "address",
            "value": "256789sc",
            "constraint": "null"
          },
          {
            "column": "longitude",
            "value": -446828,
            "constraint": "null"
          }
          ,{
            "column": "latitude",
            "value": 4469289,
            "constraint": "null"
          }
        ]
      }
    ]
  ]
}
oneToone with inserting in one reocrd
{
  "relationship": "oneTOne",
  "parentTable": "workers",
  "childTable": "location",
  "parentTable_pk": "F6272F",
  "childTable_data": [
          {
            "column": "country",
            "value": "canda",
            "constraint": "not null"
          },   {
            "column": "city",
            "value": "ontria",
            "constraint": "not null"
          },
            {
            "column": "address",
            "value": "bweya245",
            "constraint": "null"
          },
          {
            "column": "longitude",
            "value": -546228,
            "constraint": "null"
          }
          ,{
            "column": "latitude",
            "value": 5462289,
            "constraint": "null"
          }
        ]

}
