<?php

class PostgresDataConnection
{
    private $host;
    private $databaseName;
    private $userName;
    private $password;
    private $pdo;
    private static $instance = null;
    private $lastInsertedId;
    private $lastInsertedIds=[];

    private function __construct()
    {    ini_set('display_errors', 1);
         error_reporting(E_ALL);

        $this->host = "localhost";
        $this->userName = "postgres";
        $this->password = "admin2487";
        $this->databaseName = "postgres";

        try {
            $databaseSourceName = "pgsql:host={$this->host};dbname={$this->databaseName}";
            $this->pdo = new PDO($databaseSourceName, $this->userName, $this->password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }
    public function createSchema($tableName, $isThereRelationship = false, $fk_constraint = [], $data = [])
    {
        // Check if the table exists
        if (!$this->tableExists($tableName)) {
            // Generate column definitions based on data
            $columns = $this->generateColumns($data, $tableName);
            echo json_encode(["table exists" => $this->tableExists($tableName), "columns" => $columns, "data" => $data]);

            // Create the table with columns
            $sql = "
                CREATE TABLE $tableName (
                    {$tableName}_id SERIAL PRIMARY KEY,
                    year INTEGER DEFAULT DATE_PART('YEAR', CURRENT_DATE),
                    month VARCHAR(20) DEFAULT TRIM(TO_CHAR(CURRENT_DATE, 'Month')),
                    $columns,
                    created_at DATE DEFAULT CURRENT_DATE,
                    time_stamp_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                );
            ";



            echo json_encode(['queries' => $sql]);
            echo "\n";
            try {
                // Execute the SQL statement using exec or query
              $this->getInstance()->pdo->exec($sql);
                // Or using query, uncomment the line below if using query instead of exec
                // $pdo->query($sql);
            } catch (PDOException $e) {
                $errorMessage = "Table creation failed: " . $e->getMessage() . "\nSQL: " . json_encode(["sql" => $sql]);
                throw new Exception($errorMessage);
            }
        }
    }


    private function schemaForManyToMany($table, $columns,$issetContraints=false)
    {
       $primaryKeyName = "{$table}_id";
       $columnOne = $columns[0]['column'];
       $columnTwo = $columns[1]['column'];
if ($this->tableExists($table)) {
  return;
}


       // Create the table query
       $tableSql = "
           CREATE TABLE $table (
               $primaryKeyName SERIAL PRIMARY KEY,
               year INTEGER DEFAULT DATE_PART('YEAR', CURRENT_DATE),
               month VARCHAR(20) DEFAULT TRIM(TO_CHAR(CURRENT_DATE, 'Month')),
               $columnOne,
               $columnTwo,
               created_at DATE DEFAULT CURRENT_DATE,
               time_stamp_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
           );";

       try {
           $pdo = $this->getInstance()->pdo;
           $pdo->beginTransaction();
           $pdo->exec($tableSql);

           // Apply constraints using ALTER TABLE
           $constraintOne = $columns[0]['constraint'];
           $constraintTwo = $columns[1]['constraint'];
          if ($issetContraints) {
            $pdo->exec("ALTER TABLE $table ADD $constraintOne;");
            $pdo->exec("ALTER TABLE $table ADD $constraintTwo;");
          }

           $pdo->commit();

       } catch (PDOException $e) {
           throw new \Exception("Error: " . $e->getMessage());
       }
    }


    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getConnection()
    {
        return $this->pdo;
    }

    public function disconnect()
    {
        $this->pdo = null;
    }

    protected function processCondition($conditions)
    {
        $expression = 'WHERE ';
        $operator = 'AND';

        foreach ($conditions as $key => $condition) {
            $operator = $key;
            $expression .= $this->processConditionExpression($condition, $operator);
        }

        return $expression;
    }

    protected function processConditionExpression($subConditions, $operator)
    {
        $expression = '';

        foreach ($subConditions as $key => $condition) {
            $column = $key;
            $expression .= "($column = ?) $operator ";
        }

        $expression = rtrim($expression, " $operator "); // Remove trailing operator

        return $expression;
    }

    protected function extractColumnValues($conditions)
    {
        $values = [];

        foreach ($conditions as $subConditions) {
            foreach ($subConditions as $value) {
                if (is_array($value)) {
                    // Convert the array to a string representation
                    $value = implode(',', $value);
                }

                // Perform type casting if necessary
                if (is_numeric($value)) {
                    $value = (int) $value; // Convert to integer
                } elseif (is_float($value)) {
                    $value = (float) $value; // Convert to float
                }

                $values[] = $value;
            }
        }

        return $values;
    }


    public function tableExists($table)
   {
      $query = "SELECT 1 FROM $table LIMIT 1";

      try {
          $statement = $this->getInstance()->pdo->prepare($query);
          $statement->execute(); // Execute the query
          $result = $statement->fetchColumn();

          return true;
      } catch (PDOException $e) {
          return false;
      }
   }


public function alterTableAddConstraint($tableName, $fk_constraint)
 {
     $sql = "";
     foreach ($fk_constraint as $fk_name => $fk_constraint_value) {
         $sql .= "ALTER TABLE IF EXISTS $tableName ADD COLUMN IF NOT EXISTS $fk_name;";
         $sql .= "ALTER TABLE IF EXISTS $tableName ADD $fk_constraint_value;";
     }

     return $sql;
 }


  private function executeQueries($sql)
  {
      $queries = explode(";", $sql);
         echo json_encode(["queries"=>$queries]);
      $pdo = $this->getInstance()->pdo;

      foreach ($queries as $query) {
          $query = trim($query);
          if (!empty($query)) {
              $statement = $pdo->prepare($query);
              $statement->execute();
          }
      }
  }










public function generateColumns($data,$tableName)
{
   $columns = [];

   // Get all constraints from the PostgreSQL database
   $validConstraints = $this->getAllConstraints();
   echo json_encode(['data' => $data]);
   echo "\n";

   foreach ($data as $columnData) {

        $columnName = $columnData['column'];
       $value = $columnData['value'];
       $constraint = $columnData['constraint']??"null";


       $dataType = isset($columnData['dataType']) ? $columnData['dataType'] : $this->getDataType($value);
       echo   json_encode(["constraint"=>$constraint,"columnData"=>$columnData]);
       echo "\n";

       if (!$this->isValidConstraint($constraint, $validConstraints)) {

           // Invalid constraint provided by the user
           $suggestions = $this->getConstraintSuggestions($constraint, $columnName, $validConstraints);
           if (!empty($suggestions)) {
               $suggestionList = implode(', ', $suggestions);
               $errorMessage = "Invalid constraint for column '$columnName'. Did you mean: $suggestionList?";
           } else {
               $errorMessage = "Invalid constraint for column '$columnName'";
           }

           throw new Exception($errorMessage);
       }

       $columnDefinition = "$columnName $dataType $constraint";
       $columns[] = $columnDefinition;
   }

   echo json_encode(['columns' => $columns]);
   echo "\n";

   return implode(", ", $columns);
}


private function getAllConstraints()
{
   $validConstraints = [
       'NOT NULL' => 'NOT NULL',
       'UNIQUE' => 'UNIQUE',
       'NULL' => 'NULL',
       'PRIMARY KEY' => 'PRIMARY KEY',
       'FOREIGN KEY' => 'FOREIGN KEY REFERENCES table(column)',
       // Add more valid constraints with their examples if needed
   ];

   return $validConstraints;
}

private function getConstraintSuggestions($constraint, $columnName, $validConstraints)
{
   $suggestions = [];

   foreach ($validConstraints as $validConstraint) {
       // Check if the invalid constraint matches a known pattern
       if (preg_match('/' . preg_quote($validConstraint, '/') . '/i', $constraint)) {
           $suggestions[] = $validConstraint;
       }
   }

   // Additional suggestion for primary key constraint
   if (strpos($constraint, 'PRIMARY KEY') !== false) {
       $suggestions[] = "The column '{$columnName}_id' already uses the primary key constraint. Consider using a combination of 'NOT NULL' and 'UNIQUE' constraints instead.";
       $suggestions[] = "Example: '{$columnName} NOT NULL UNIQUE'";
   }

   // Additional suggestion for foreign key constraint
   if (strpos($constraint, 'FOREIGN KEY') !== false) {
       $suggestions[] = "Use the provided relationship methods in the model to define the foreign key constraint.";
       $suggestions[] = "Check the syntax and reference table/column for the foreign key constraint.";
   }

   // Additional suggestion for misspelled constraints
   if (!empty($suggestions)) {
       $suggestions[] = "Make sure the constraint is spelled correctly and matches one of the valid constraints.";
   }

   return $suggestions;
}

private function isValidConstraint(&$constraint, $validConstraints)
{
   $constraint = strtoupper($constraint);

   return in_array($constraint, $validConstraints);
}



private function handleSqlForBackupBooleans($tableName)
{
    $sql = <<<SQL
ALTER TABLE IF EXISTS "{$tableName}"
ADD COLUMN IF NOT EXISTS is_deleted BOOLEAN DEFAULT false,
ADD COLUMN IF NOT EXISTS is_updated BOOLEAN DEFAULT false;
SQL;
    return $sql;
}




private function validateDataFormat($relationShipData)
{
   $requiredKeys = ['parentTable', 'childTable', 'childTable_data'];
   $optionalKeys = ['parentTable_data', 'parentTable_pk'];

   foreach ($requiredKeys as $key) {
       if (!isset($relationShipData[$key])) {
           throw new Exception("Missing required key: $key");
       }
   }

   $isParentTableDataSet = isset($relationShipData['parentTable_data']);
   $isParentTablePKSet = isset($relationShipData['parentTable_pk']);

   if ($isParentTableDataSet && $isParentTablePKSet) {
       throw new Exception("Both parentTable_data and parentTable_PK are set. Please provide only one of them.");
   }

   if (!$isParentTableDataSet && !$isParentTablePKSet) {
       throw new Exception("Either parentTable_data or parentTable_PK should be set.");
   }

   if ($isParentTableDataSet) {
       if (!is_array($relationShipData['parentTable_data'])) {
           throw new Exception("Invalid parentTable_data format. Please provide an array.");
       }

       $this->validateTableData($relationShipData['parentTable_data']);
   }

   if (!is_array($relationShipData['childTable_data'])) {
       throw new Exception("Invalid childTable_data format. Please provide an array.");
   }

   $this->validateTableData($relationShipData['childTable_data']);

   return true;
}

private function validateTableData($tableData)
{
  echo json_encode(['table data' =>$tableData]);
  echo "\n";
   foreach ($tableData as $data) {

       $requiredKeys = ['column', 'value'];
       foreach ($requiredKeys as $key) {

           if (!array_key_exists($key, $data)) {
               throw new Exception("Invalid table data format. '{$key}' key is required.");
           }
       }

       // Constraint is optional, so only validate if it exists
       if (isset($data['constraint']) && !is_string($data['constraint'])) {
           throw new Exception("Invalid table data format. 'constraint' should be a string.");
       }



   }
}
public function oneToOneRelationship($relationshipData)
{
    $this->validateDataFormat($relationshipData);

    $parentTable = $relationshipData['parentTable'];
    $isCascade = $relationshipData['cascade'] ?? false;
    $isBackUp = $relationshipData['backup'] ?? false;
    $cascade = $isCascade ? "ON DELETE CASCADE" : "";

    $childTable = $relationshipData['childTable'];
    $parentTableData = $relationshipData['parentTable_data'] ?? [];
    $childTableData = $relationshipData['childTable_data'];

    $primaryKeyName = "{$parentTable}_id";
    $foreignKeyName = "{$primaryKeyName} INTEGER";
    $foreignKeyColumn = "{$primaryKeyName}";
    $constraintName = "fk_{$foreignKeyColumn}"; // Add prefix "fk_" to the constraint name
    $constraintMetadata = "CONSTRAINT {$constraintName} FOREIGN KEY ({$foreignKeyColumn}) REFERENCES {$parentTable} ({$foreignKeyColumn}) {$cascade}";

    $constraints = [$foreignKeyName => $constraintMetadata];

    if (!$this->tableExists($parentTable)) {
        $this->createSchema($parentTable, false, [], $parentTableData);
        if ($isBackUp) {
            $this->createSchema("{$parentTable}_historical", false, [], $parentTableData);
            $sql = $this->handleSqlForBackupBooleans("{$parentTable}_historical");

            try {

                $this->tableAlteration($sql);
                $this->setTableUPDATEAndDeleteBackups($parentTable);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
    }

    if (!$this->tableExists($childTable)) {
        $this->createSchema($childTable, false, $constraints, $childTableData);
        $this->createSchema("{$childTable}_historical", false, [], $childTableData);
          $this->addColumn("{$childTable}_historical", $foreignKeyName);
        $this->executeQueries($this->alterTableAddConstraint($childTable, $constraints));

        if ($isBackUp) {
            $this->createSchema("{$childTable}_historical", false, $childTableData);
            $sql = $this->handleSqlForBackupBooleans("{$childTable}_historical");

            try {
                $this->tableAlteration($sql);
                $this->setTableUPDATEAndDeleteBackups($childTable);
            } catch (\Exception $e) {
                throw new \Exception("Error occurred in oneToOneRelationship(): " . $e->getMessage());
            }
        }
    }

    $parentTableId = isset($relationshipData['parentTable_pk']) ? $relationshipData['parentTable_pk'] : $this->insertDataIntoTable($parentTableData, $parentTable);

    $fk = [$foreignKeyColumn => $parentTableId];

    foreach ($fk as $key => $value) {
        $childTableData[] = ["column" => $key, "value" => $value, "constraint" => "NULL UNIQUE"];
    }

    $childTableId = $this->insertDataIntoTable($childTableData, $childTable);
    $this->lastInsertedIds = ["{$parentTable}_id" => $parentTableId, "{$childTable}_id" => $childTableId];
    return $this->lastInsertedIds;
}

public function oneToManyRelationship($relationShipData)
{
    $this->validateDataFormat($relationShipData);

    $parentTable = $relationShipData['parentTable'];
    $childTable = $relationShipData['childTable'];
    $parentTableData = $relationShipData['parentTable_data'] ?? null;
    $childTableData = $relationShipData['childTable_data'] ?? null;
    $foreignKeyColumn = "{$parentTable}_id";
    $foreignKeyName = "{$foreignKeyColumn} INTEGER";

    $cascade = $relationShipData['cascade'] ?? false;
    $isBackup = $relationShipData['backup'] ?? false;

    echo json_encode(['cascade' => $cascade, "backup" => $isBackup]);

    $constraint = [$foreignKeyName => "CONSTRAINT oneToMany_fk_{$foreignKeyColumn} FOREIGN KEY ({$foreignKeyColumn}) REFERENCES {$parentTable} ({$foreignKeyColumn})"];

    if ($cascade) {
        $constraint[$foreignKeyName] .= " ON DELETE CASCADE";
    }

    if (!$this->tableExists($parentTable)) {
        $this->createSchema($parentTable, false, [], $parentTableData);

        if ($isBackup) {
            $this->createSchema("{$parentTable}_historical", false, [], $parentTableData);
            $sql = $this->handleSqlForBackupBooleans("{$parentTable}_historical");
            try {
               echo json_encode(["foreignKey" =>$foreignKeyName]);

                $this->tableAlteration($sql);
                $this->setTableUPDATEAndDeleteBackups($parentTable);
            } catch (\Exception $e) {
                throw new \Exception("Error occurred in oneToManyRelationship(): " . $e->getMessage());
            }
        }
    }

    if (!$this->tableExists($childTable)) {
        $this->createSchema($childTable, false, $constraint, $childTableData);
        $this->executeQueries($this->alterTableAddConstraint($childTable, $constraint));

        if ($isBackup) {
            $this->createSchema("{$childTable}_historical", false, [], $childTableData);
            $sql = $this->handleSqlForBackupBooleans("{$childTable}_historical");
            try {
                $this->addColumn("{$childTable}_historical", $foreignKeyName);
                $this->tableAlteration($sql);
                $this->setTableUPDATEAndDeleteBackups($childTable);
            } catch (\Exception $e) {
                throw new \Exception("Error occurred in oneToManyRelationship(): " . $e->getMessage());
            }
        }
    }

    if ($this->tableExists($parentTable) && $this->tableExists($childTable)) {
      $parentTableId = isset($relationShipData['parentTable_pk']) ? $relationShipData['parentTable_pk'] : $this->insertDataIntoTable($parentTableData, $parentTable);

      $childTableData[] = ["column" => $foreignKeyColumn, "value" => $parentTableId, "constraint" => "NOT NULL"];
      $childTableId = $this->insertDataIntoTable($childTableData, $childTable);

      $this->lastInsertedIds = ['parentTableId' => $parentTableId, 'childTableId' => $childTableId];
    }


    return $this->lastInsertedIds;
}
public function manyToManyRelationship($relationShipData)
{
   $this->validateManyToManyDataFormat($relationShipData);

   $parentTableOne = $relationShipData['parentTableOne'];
   $parentTableTwo = $relationShipData['parentTableTwo'];
   $parentTableOneData = $relationShipData['parentTableOne_data'] ?? [];
   $parentTableTwoData = $relationShipData['parentTableTwo_data'] ?? [];
   $foreignKeyOne = "{$parentTableOne}_id";
   $foreignKeyTwo = "{$parentTableTwo}_id";
   $foreignKeyOneColumn = "{$parentTableOne}_id";
   $foreignKeyTwoColumn = "{$parentTableTwo}_id";
   $junctionTable = "{$parentTableOne}_{$parentTableTwo}";

   $cascade = $relationShipData['cascade'] ?? false;
   $isBackup = $relationShipData['backup'] ?? false;

   if (!$this->tableExists($parentTableOne)) {
       $this->createSchema($parentTableOne, false, [], $parentTableOneData);
       if ($isBackup) {
           $this->createSchema("{$parentTableOne}_historical", false, [], $parentTableOneData);
           $sql = $this->handleSqlForBackupBooleans("{$parentTableOne}_historical");
           try {
               $this->setTableUPDATEAndDeleteBackups($parentTableOne);
               $this->tableAlteration($sql);
           } catch (\Exception $e) {
               throw new \Exception("Error occurred in manyToManyRelationship(): " . $e->getMessage());
           }
       }
   }

   if (!$this->tableExists($parentTableTwo)) {
       $this->createSchema($parentTableTwo, false, [], $parentTableTwoData);
       if ($isBackup) {
           $this->createSchema("{$parentTableTwo}_historical", false, [], $parentTableTwoData);
           $sql = $this->handleSqlForBackupBooleans("{$parentTableTwo}_historical");
           try {
               $this->setTableUPDATEAndDeleteBackups($parentTableTwo);
               $this->tableAlteration($sql);
           } catch (\Exception $e) {
               throw new \Exception("Error occurred in manyToManyRelationship(): " . $e->getMessage());
           }
       }
   }

   if (!$this->tableExists($junctionTable)) {
       $junctionMetaData = [
           "foreignKeyOneColumn" => $foreignKeyOneColumn,
           "foreignKeyTwoColumn" => $foreignKeyTwoColumn,
           "parentTableOne" => $parentTableOne,
           "parentTableTwo" => $parentTableTwo
       ];
       $junctionTableColumns = $this->junctionTableColumns($junctionMetaData);

       // Use array_map to modify the $junctionTableColumns array
       $junctionTableColumnsModified = array_map(function ($column) {
           $column["constraint"] .= " ON DELETE CASCADE";
           return $column;
       }, $junctionTableColumns);

       try {
           $this->schemaForManyToMany($junctionTable, $cascade ? $junctionTableColumnsModified : $junctionTableColumns,true);
       } catch (\Exception $e) {
           throw new \Exception($e->getMessage());
       }
       if ($isBackup) {

           $backupparentTableOne = "{$parentTableOne}_historical";
           $backupparentTableTwo = "{$parentTableTwo}_historical";

           $junctionMetaData = [
               "foreignKeyOneColumn" => $foreignKeyOneColumn,
               "foreignKeyTwoColumn" => $foreignKeyTwoColumn,
               "parentTableOne" => $backupparentTableOne,
               "parentTableTwo" => $backupparentTableTwo
           ];
           $backupjunctionTableColumns = $this->junctionTableColumns($junctionMetaData);
           $backupjunctionTableColumnsModified = array_map(function ($column) {
               $column["constraint"] .= " ON DELETE CASCADE";
               return $column;
           }, $backupjunctionTableColumns);

           $this->schemaForManyToMany("{$junctionTable}_historical", $cascade ? $backupjunctionTableColumnsModified : $backupjunctionTableColumns);
           $sql = $this->handleSqlForBackupBooleans("{$junctionTable}_historical");

           try {
               $this->setTableUPDATEAndDeleteBackups($junctionTable);
               $this->tableAlteration($sql);
           } catch (\Exception $e) {
               throw new \Exception("Error occurred in schema: " . $e->getMessage());
           }
       }
   }





    $recordIdOne = isset($relationShipData['parentTableOne_pk']) ? $relationShipData['parentTableOne_pk'] : $this->insertDataIntoTable($parentTableOneData, $parentTableOne);
    $recordIdTwo = isset($relationShipData['parentTableTwo_pk']) ? $relationShipData['parentTableTwo_pk'] : $this->insertDataIntoTable($parentTableTwoData, $parentTableTwo);

    $relationshipData = [
        ['column' => $foreignKeyOneColumn, 'value' => $recordIdOne],
        ['column' => $foreignKeyTwoColumn, 'value' => $recordIdTwo]
    ];

    $lastInsertedId = $this->insertDataIntoTable($relationshipData, $junctionTable);
    $this->lastInsertedIds = ["{$parentTableOne}_id" => $recordIdOne, "{$parentTableTwo}_id" => $recordIdTwo];

    return $this->lastInsertedIds;
}
// Function to add the cascade option for DELETE

public function lastInsertedIds()
{
    return $this->lastInsertedIds;
}


private function validateManyToManyDataFormat($relationShipData)
{
   $requiredKeys = ['parentTableOne', 'parentTableTwo'];
   $optionalKeys = ['parentTableOne_pk', 'parentTableTwo_pk', 'parentTableOne_data', 'parentTableTwo_data'];

   foreach ($requiredKeys as $key) {
       if (!isset($relationShipData[$key])) {
           throw new Exception("Missing required key: '$key'");
       }
   }

   $isParentTableOneDataSet = isset($relationShipData['parentTableOne_data']);
   $isParentTableTwoDataSet = isset($relationShipData['parentTableTwo_data']);
   $isParentTableOnePKSet = isset($relationShipData["parentTableOne_pk"]);
   $isParentTableTwoPKSet = isset($relationShipData["parentTableTwo_pk"]);

   if ($isParentTableOneDataSet && $isParentTableOnePKSet) {
       throw new Exception("Only one of 'parentTableOne_data' or 'parentTableOne_pk' should be set.");
   }

   if ($isParentTableTwoDataSet && $isParentTableTwoPKSet) {
       throw new Exception("Only one of 'parentTableTwo_data' or 'parentTableTwo_pk' should be set.");
   }

   // Validate table data
   if (!$isParentTableOnePKSet) {
       $this->validateTableData($relationShipData['parentTableOne_data']);
   } if(!$isParentTableTwoPKSet ) {
       $this->validateTableData($relationShipData['parentTableTwo_data']);
   }
}

private function junctionTableColumns($junctionMetaData)
{
   $foreignKeyOneColumn = $junctionMetaData["foreignKeyOneColumn"] ?? "";
   $parentTableOne = $junctionMetaData["parentTableOne"] ?? "";
   $foreignKeyTwoColumn = $junctionMetaData["foreignKeyTwoColumn"] ?? "";
   $parentTableTwo = $junctionMetaData["parentTableTwo"] ?? "";

   $junctionTableColumns = [
       [
           "column" => "{$foreignKeyOneColumn} INTEGER",
           "constraint" => "CONSTRAINT manyToMany_fk_{$foreignKeyOneColumn} FOREIGN KEY ({$foreignKeyOneColumn}) REFERENCES {$parentTableOne} ({$foreignKeyOneColumn})"
       ],
       [
           "column" => "{$foreignKeyTwoColumn} INTEGER",
           "constraint" => "CONSTRAINT manyToMany_fk_{$foreignKeyTwoColumn} FOREIGN KEY ({$foreignKeyTwoColumn}) REFERENCES {$parentTableTwo} ({$foreignKeyTwoColumn})"
       ]
   ];
   return $junctionTableColumns;
}



private function generateManyToManyExampleFormatArray()
{
   return [
       'relationshipTable' => 'relationship_table_name',
       'parentTableOne' => 'parent_table_one_name',
       'parentTableTwo' => 'parent_table_two_name',
       'parentTableOne_data' => [
           ['column' => 'column_name', 'value' => 'value', 'constraint' => 'constraint'],
           // Add more columns and values as needed
       ],
       'parentTableTwo_data' => [
           ['column' => 'column_name', 'value' => 'value', 'constraint' => 'constraint'],
           // Add more columns and values as needed
       ]
   ];
}

private function generateManyToManyExampleFormatJson()
{
   $exampleFormatArray = $this->generateManyToManyExampleFormatArray();
   return json_encode($exampleFormatArray, JSON_PRETTY_PRINT);
}





private function getDataType($value)
{
    if (is_int($value)) {
        return 'integer';
    } elseif (is_bool($value)) {
        return 'boolean';
    } elseif (is_float($value)) {
        return 'float';
    } elseif (is_string($value)) {
        $length = strlen($value);

        if ($length > 255) {
            return 'text';
        } elseif ($length > 50) {
            return 'varchar(500)';
        } else {
            return 'varchar(255)';
        }
    }

    return 'varchar(255)';
}
private function extractColumnValuesAsKeys($data)
{
    $extractedData = [];
    foreach ($data as $field) {

         $columnName = $field['column'];
         $columnValue = $field['value'];
          $extractedData[$columnName] = $columnValue;


    }
    return $extractedData;
}

private function insertDataIntoTable($data,$table)
{    $primaryKeyColumn = "{$table}_id";
    if (!$this->tableExists($table)) {
        try {
          $this->createSchema($data);
        } catch (\Exception $e) {
          throw new \Exception("error:".$e->getMessage());

        }

    }

    $columnNames = $this->getColumnNames($table);
    $orderedData = $this->reorderDataFields($this->extractColumnValuesAsKeys($data), $columnNames);
    $this->addMissingColumns($columnNames,$data,$table,$orderedData);
    echo json_encode(["orderedData"=>$orderedData]);
    echo "\n";
      echo json_encode(["columns"=>$columnNames]);
      echo "\n";

    $columns = implode(", ", array_keys($orderedData));
    $values = implode(", ", array_fill(0, count($orderedData), "?"));
    $sql = "INSERT INTO $table ($columns) VALUES ($values) RETURNING $primaryKeyColumn";

    try {
        $statement = $this->getInstance()->pdo->prepare($sql);
        $statement->execute(array_values($orderedData));
         $lastInsertedId = $statement->fetchColumn();
        return $lastInsertedId;
    } catch (PDOException $e) {
        throw new Exception("Insertion failed: " . $e->getMessage());
    }
}

public function getRelationshipType($relationshipType)
{
    $relationTypes = ['none','oneToOne', 'oneToMany', 'manyToMany'];

    if (in_array($relationshipType, $relationTypes)) {
        return $relationshipType;
    }
    return false;
}


protected function handleRelationship($relationsType, $relationship_meta_data)
{
    switch ($relationsType) {
        case 'oneToOne':
            $this->oneToOneRelationship($relationship_meta_data);
            break;
        case 'oneToMany':
            $this->oneToManyRelationship($relationship_meta_data);
            break;
        case 'manyToMany':
            $this->manyToManyRelationship($relationship_meta_data);
            break;
      case 'none':
              $this->tableWithNoRelationShip($relationship_meta_data);
              break;
        default:
            echo json_encode(["error" => "No relationship is defined"]);
            break;
    }
}

private function addMissingColumns(&$columnNames, &$data, $table, &$orderedData)
{
    foreach ($data as $value) {
        if (!in_array($value['column'], $columnNames)) {
            $dataType = $this->getDataType($value['value']);
            $meta_data = ["tableName" => $table, "dataType" => $dataType, "column" => $value['column']];
            $this->alterTable($meta_data);
            $meta_data["tableName"] = "{$table}_historical";
            $this->alterTable($meta_data);
            $columnNames[] = $value['column'];
            $orderedData[$value['column']] = $value['value'];
        }
    }
}


protected function getColumnNames($tableName)
{
    $query = "SELECT column_name FROM information_schema.columns WHERE table_name = ?";

    try {
        $statement = $this->getInstance()->pdo->prepare($query);
        $statement->execute([$tableName]);
        $result = $statement->fetchAll(PDO::FETCH_COLUMN);
        return $result;
    } catch (PDOException $e) {
        throw new Exception("Failed to retrieve column names: " . $e->getMessage());
    }
}

private function reorderDataFields($data, $columnNames)
{
   $commonKeys = array_intersect($columnNames, array_keys($data));
   $orderedData = array_intersect_key($data, array_flip($commonKeys));
   return $orderedData;
}


private function alterTable($meta_data)
{   $tableName =$meta_data["tableName"];
    $dataType = $meta_data["dataType"];
    $columnName= $meta_data["column"];

    $query = "ALTER TABLE $tableName ADD COLUMN $columnName $dataType NULL";

    try {
      $this->getInstance()->pdo->exec($query);
    } catch (PDOException $e) {
        throw new Exception("Failed to alter table: " . $e->getMessage());
    }
}

protected function renameAndSetColumnNames($tableName, $isRelated = null)
{
   if ($isRelated === null) {
       $isRelated = $this->hasRelationship($tableName);
   }

   $columns = $this->getColumnNames($tableName);
   $renameColumns = [];

   foreach ($columns as $value) {
       if ($isRelated) {
           $renameColumns[] = $tableName . '.' . $value . ' AS ' . $tableName . '_' . $value;
       } else {
           $renameColumns[] = $value . ' AS ' . $tableName . '_' . $value;
       }
   }

   echo json_encode(["columns" => $renameColumns]);
   echo "\n";

   return implode(",", $renameColumns);
}





private function generateExampleFormatArray()
{
    return [
        'parentTable' => 'workers',
        'childTable' => 'address',
        'parentTable_data' => [
            [
                'column' => 'column_name',
                'value' => 'column_value',
                'constraint' => 'not null'
            ],
            // Add more columns if needed
        ],
        'childTable_data' => [
            [
                'column' => 'column_name',
                'value' => 'column_value',
                'constraint' => 'not null'
            ],
            // Add more columns if needed
        ],
    ];
}

private function generateExampleFormatJson($exampleFormatArray)
{
    return json_encode($exampleFormatArray, JSON_PRETTY_PRINT);
}

public function tableWithNoRelationShip($data)
{
   // Extract table name and table data from the input data
   $tableName = $data['table'];
   $tableData = $data['table_data'];

   // Check if the table data is set and validate its format
   if (isset($tableData)) {
       $this->validateTableData($tableData);
   }

   // Check if the table exists
   if (!$this->tableExists($tableName)) {
       // Create the table
       $this->createSchema($tableName, false, [], $tableData);

       $isBackup = $data['backup'] ?? false;
       if ($isBackup) {
           $this->createSchema("{$tableName}_historical", false, [], $tableData);
           $sql =  $this->handleSqlForBackupBooleans("{$tableName}_historical");

            try {
              $this->tableAlteration($sql);
            } catch (\Exception $e) {
              throw new \Exception("Error occurred in tableWithNoRelationShip(): " . $e->getMessage());
           }
           try {
               $this->setTableUPDATEAndDeleteBackups($tableName);
           } catch (\Exception $e) {
                   throw new \Exception("Error occurred in  tableWithNoRelationShip(): " . $e->getMessage());
           }
       }
   }

   // Insert data into the table
   $recordId = $this->insertDataIntoTable($tableData, $tableName);
   $id = $recordId;
   $this->lastInsertedId = $id;

   return ["{$tableName}_id" => $id];
}

private function getTableConstraint($table) {
    $sql = "SELECT conname, conrelid::regclass AS table_name, confrelid::regclass AS referenced_table_name
            FROM pg_constraint
            WHERE confrelid = ?::regclass";

    $statement = $this->getInstance()->pdo->prepare($sql);
    $statement->execute([$table]);
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode(["results" => $results]);
    echo "\n";

    return $results;
}

public function fetch($data) {
    $table = $data['table'];
    $conditions = $data['conditions'];
    $this->getTableConstraint($table);
}


private function isParentTable($tableName)
{
  $sql = "SELECT conname, conrelid::regclass AS table_name, confrelid::regclass AS referenced_table_name
          FROM pg_constraint
          WHERE confrelid = ?::regclass";

  $statement = $this->getInstance()->pdo->prepare($sql);
  $statement->execute([$tableName]);
  $results = $statement->fetchAll(PDO::FETCH_ASSOC);
  $rowCount = $statement->rowCount();

  return $rowCount > 0;
}

protected function hasRelationship($tableName)
{
   $sql = "SELECT table_name,
       CASE
           WHEN EXISTS (
               SELECT 1
               FROM information_schema.table_constraints
               WHERE (table_name = ?
                   OR constraint_name IN (
                       SELECT constraint_name
                       FROM information_schema.constraint_table_usage
                       WHERE table_name = ?
                   ))
                   AND constraint_type = 'FOREIGN KEY'
                   AND table_schema = 'public'
           ) THEN true
           ELSE false
       END AS has_relationship
       FROM information_schema.tables
       WHERE table_name = ?
       AND table_schema = 'public'";

   $statement = $this->getInstance()->pdo->prepare($sql);
   $statement->execute([$tableName, $tableName, $tableName]);
   $result = $statement->fetch(PDO::FETCH_ASSOC);

   return $result["has_relationship"] === true ? true : false;
}


private function foreignKeys($tableName)
{
 $sql = "SELECT DISTINCT pg_class.relname
         FROM pg_attribute
         JOIN pg_constraint ON pg_attribute.attnum = ANY (pg_constraint.conkey)
         JOIN pg_class ON pg_class.oid = pg_constraint.confrelid
         WHERE pg_attribute.attrelid = ?::regclass
           AND pg_constraint.contype = 'f'
           AND pg_constraint.confrelid = ?::regclass";

 $statement = $this->getInstance()->pdo->prepare($sql);
 $statement->execute([$tableName, $tableName]);
 $results = $statement->fetchAll(PDO::FETCH_ASSOC);

 $tableNames = array_column($results, 'relname');

 return $tableNames;
}



private function getParentTable($tableName)
{
  $sql = "SELECT conname, conrelid::regclass AS table_name, confrelid::regclass AS referenced_table_name
          FROM pg_constraint
          WHERE confrelid = ?::regclass";

  $statement = $this->getInstance()->pdo->prepare($sql);
  $statement->execute([$tableName]);
  $results = $statement->fetchAll(PDO::FETCH_ASSOC);
  echo json_encode(["results" => $results]);
  echo "\n";
  return $results;
}

private function getTableNamesOfChildTable($tableName)
{

  $results = $this->foreignKeys($tableName);
 echo json_encode(["table names"=>$results]);
 echo "\n";
 return $results;
}
private function getRelatedTables($tableName)
{
    $sql = "
        SELECT
            conname AS constraint_name,
            conrelid::regclass AS child_table,
            confrelid::regclass AS parent_table
        FROM
            pg_constraint
        WHERE
            (conrelid = ?::regclass OR confrelid = ?::regclass)
            AND confrelid != 0;
    ";

    $pdo = $this->getInstance()->pdo;
    $statement = $pdo->prepare($sql);
    $statement->execute([$tableName, $tableName]);

    return $statement->fetchAll(PDO::FETCH_ASSOC);
}
protected function processedConditions(array $conditions)
{
   $condition = "";
   $params = [];
   $operator = 'AND'; // Default operator

   foreach ($conditions as $item) {
       if (is_array($item)) {
           $innerResult = $this->processedConditions($item);
           $innerCondition = $innerResult['condition'];

           $condition .= ($condition !== "" ? " {$operator} " : "") . "($innerCondition)";
           $params = array_merge($params, $innerResult['params']);
       } elseif (strtolower($item) === 'or' || strtolower($item) === 'and') {
           $operator = strtoupper($item);
       }
   }

   if (isset($conditions['column'], $conditions['operator'], $conditions['value'])) {
       $column = $conditions['column'];
       $operator = OPERATORS[$conditions['operator']] ?? null;
       $value = $conditions['value'];

       if ($column !== null && $operator !== null) {
           $placeholder = "?";
           $condition .= ($condition !== "" ? " {$operator} " : "") . "{$column} {$operator} {$placeholder}";
           $params[] = $value;
       }
   }

   return ['condition' => $condition, 'params' => $params];
}


protected function getTableNames($tableName, $isManyToMany=false, $isConditionSet,$isHistorical=false)
{     $filteredData =[];
  if (!$isHistorical) {
    $filteredData = array_filter($this->getRelatedTables($tableName), function ($row) {
    return stripos($row["child_table"], "historical") === false;
});
} else {
   $filteredData = $this->getRelatedTables($tableName);
}

   if ($this->hasRelationship($tableName)) {
       $selectedTables = [];
       $otherParentTable = "";

       if ($isManyToMany && $isConditionSet) {
           $junctionTable = $filteredData[0]["child_table"];
           $resultsTable = $this->getRelatedTables($junctionTable);

           foreach ($resultsTable as &$value) {
               if ($tableName !== $value["parent_table"]) {
                   $otherParentTable = $value["parent_table"];
               }
           }

           foreach ($resultsTable as $value) {
               if ($tableName === $value["parent_table"]) {
                   $value["other_parent_table"] = $otherParentTable;
                   $selectedTables[] = $value;
               }
           }

           return $selectedTables;
       } elseif ($isManyToMany) {
           $junctionTable = $filteredData[0]["child_table"];
           $resultsTable = $this->getRelatedTables($junctionTable);
           return $resultsTable;
       } else {
           return $filteredData;
       }
   } else {
       echo "Table has no relationship\n";
       return ["parent_table" => $tableName];
   }
}




protected function buildPreparedCondition(array $conditions, &$tableName,int &$index = 1): array
{
  $condition = "";
  $params = [];
  $operator = 'AND'; // Default operator

  foreach ($conditions as $item) {
      if (is_array($item)) {

          $innerResult = $this->buildPreparedCondition($item,$tableName,$index);
          $innerCondition = $innerResult['condition'];
          $innerParams = $innerResult['params'];

          $condition .= ($condition !== "" ? " {$operator} " : "") . "($innerCondition)";
          $params = array_merge($params, $innerParams);
      } elseif (strtolower($item) === 'or' || strtolower($item) === 'and') {
          $operator = strtoupper($item);
      }
  }

  if (isset($conditions['column'], $conditions['value'], $conditions['operator'])) {
      $column =  $conditions['column'];
      $value = $conditions['value'];
      $operator = OPERATORS[$conditions['operator']] ?? null;

      if ($column !== null && $value !== null && $operator !== null) {
          $paramName = ":param{$index}";
          $condition .= ($condition !== "" ? " {$operator} " : "") . "{$column} {$operator} {$paramName}";
          $params[$paramName] = $value;
          $index++;
      }
  }

  return [
      'condition' => $condition,
      'params' => $params
  ];
}

protected function getOneTable($tableName)
{
    if (!$this->tableExists($tableName)) {
        throw new \Exception("The table you provided does not exist");
    }

    foreach ($this->getTableNames($tableName,false,false) as $tables) {
      echo json_encode(["table"=>$this->getTableNames($tableName,false,false)]);


        foreach ($tables as $key => $value) {
            if ($value === $tableName) {
                return $tables;
            }
        }
    }

    // Return null or throw an exception if the table name is not found in the specified database.
    return null; // You can adjust this based on your use case.
}




protected  function lastInsertedId()
{
  return $this->lastInsertedId;
}

private function createTriggerFunctionForDelete($tableName)
{
    $trigger = <<<SQL
CREATE TRIGGER backup_{$tableName}_delete
AFTER DELETE ON $tableName
FOR EACH ROW
EXECUTE FUNCTION backup_{$tableName}();
SQL;
    return $trigger;
}

private function createTriggerFunctionForUpdate($tableName)
{
    $trigger = <<<SQL
CREATE TRIGGER backup_{$tableName}_update
AFTER UPDATE ON $tableName
FOR EACH ROW
EXECUTE FUNCTION backup_{$tableName}();
SQL;
    return $trigger;
}

private function triggerFunctionForSQL($tableName, $columnNamesOfOriginalTables)
{
   $column = "{$tableName}_id INTEGER";
   $backupTable = "{$tableName}_historical";
   $column_meta_data = [
       "tableName" => $backupTable,
       "dataType" => "TIMESTAMP",
       "defaultValue" => "CURRENT_TIMESTAMP",
       "column" => "backedup_at"
   ];

   try {
       $this->addColumn($backupTable, $column);
       $this->addColumnAndSetDefaultValue($column_meta_data); // Pass the $column_meta_data to the function
   } catch (\Exception $e) {
       throw new \Exception($e->getMessage());

   }

   $filteredColumns = array_filter($columnNamesOfOriginalTables, function ($item) use ($tableName) {
       return $item !== "{$tableName}_id";
   });
   $columnNamesPrepared = implode(", ", $filteredColumns);

   // Modify the column names to include "OLD." prefix
   $modifiedColumnNames = implode(", ", array_map(function ($column) {
       return "OLD.{$column}";
   }, $filteredColumns));

   $triggerFunctionSQL = <<<SQL
CREATE OR REPLACE FUNCTION backup_{$tableName}()
RETURNS TRIGGER AS $$
BEGIN
 IF (TG_OP = 'DELETE') THEN
     INSERT INTO $backupTable ($columnNamesPrepared, {$tableName}_id, is_deleted, is_updated) VALUES ($modifiedColumnNames, OLD.{$tableName}_id, TRUE, FALSE);
 ELSIF (TG_OP = 'UPDATE') THEN
     INSERT INTO $backupTable ($columnNamesPrepared, {$tableName}_id, is_deleted, is_updated) VALUES ($modifiedColumnNames, OLD.{$tableName}_id, FALSE, TRUE);
 END IF;
 RETURN NULL;
END;
$$
LANGUAGE plpgsql;
SQL;

   return $triggerFunctionSQL;
}


protected function addColumn($tableName, $column)
{
   $sql = "ALTER TABLE $tableName ADD COLUMN IF NOT EXISTS $column";

   $pdo = $this->getInstance()->pdo; // Replace "getInstance()" with your valid method to get the database connection.

   try {
       $pdo->exec($sql);
   } catch (\PDOException $e) {
       throw new \Exception($e->getMessage());
   }
}

private function setTableUPDATEAndDeleteBackups($tableName)
{
   // Get the column names as a comma-separated string
   $columnNamesOfBackupTable = $this->getColumnNames("{$tableName}_historical");
   $columnNamesOfOriginalTables = $this->getColumnNames($tableName);
   $triggerFunctionSQL = $this->triggerFunctionForSQL($tableName,$columnNamesOfOriginalTables);
   $triggerForDelete = $this->createTriggerFunctionForDelete($tableName);
   $triggerForUpdate = $this->createTriggerFunctionForUpdate($tableName);

   try {
       $pdo = $this->getInstance()->pdo;
       $pdo->beginTransaction();

       // Create the trigger function for SQL
       $pdo->exec($triggerFunctionSQL);

       // Create the trigger function for DELETE
       $pdo->exec($triggerForDelete);

       // Create the trigger function for UPDATE
       $pdo->exec($triggerForUpdate);

       // Commit the transaction
       $pdo->commit();
   } catch (PDOException $pdoException) {
       // Rollback the transaction on PDO exception
       $pdo->rollBack();
       throw new Exception("PDO Exception while setting table UPDATE and DELETE backups: " . $pdoException->getMessage());
   } catch (Exception $exception) {
       // Rollback the transaction on other exceptions
       $pdo->rollBack();
       throw new Exception("Exception while setting table UPDATE and DELETE backups: " . $exception->getMessage());
   }
}


private function tableAlteration($sql)
{     echo json_encode(["sql backup"=>$sql]);
      echo "\n";
    try {
      $this->getInstance()->pdo->exec($sql);
    } catch (PDOException $pdoException) {
        throw new Exception("PDO Exception: " . $pdoException->getMessage());
    } catch (Exception $exception) {
        throw new Exception("Exception: " . $exception->getMessage());
    }
}

protected function addColumnAndSetDefaultValue($column_meta_data)
{
    $tableName = $column_meta_data["tableName"];
    $dataType = $column_meta_data["dataType"];
    $defaultValue = $column_meta_data["defaultValue"];
    $column = $column_meta_data["column"];

    // Add double quotes around the table and column names
    $tableName = "\"$tableName\"";
    $column = "\"$column\"";

    $sql = "ALTER TABLE $tableName ADD COLUMN $column $dataType DEFAULT $defaultValue";

    try {
        $this->getInstance()->pdo->exec($sql);
    } catch (PDOException $e) {
        throw new \Exception($e->getMessage());
    }
}




 }

?>
