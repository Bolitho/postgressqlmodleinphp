 Here is the JSON representation of the example:


{
  "relationship": "oneToOne",
  "parentTable": "workers",
  "childTable": "address",
  "parentTable_data": [
    {
      "column": "firstname",
      "value": "John",
      "constraint": "not null"
    },
    {
      "column": "lastname",
      "value": "Doe",
      "constraint": "not null"
    },
    {
      "column": "phonenumber",
      "value": null,
      "constraint": "null"
    },
    {
      "column": "email",
      "value": null,
      "constraint": "null"
    }
  ],
  "childTable_data": [
    {
      "column": "country",
      "value": null,
      "constraint": "not null"
    },
    {
      "column": "city",
      "value": null,
      "constraint": "not null"
    },
    {
      "column": "address",
      "value": null,
      "constraint": "null"
    }
  ]
}
```

Developers can use this JSON format as a request payload to configure the one-to-one relationship between the "workers" and "address" tables.

php format
Developers can use this PHP array as a request payload to configure the one-to-one relationship between the "workers" and "address" tables in their PHP application.

$requestPayload = [
    "relationship" => "oneToOne",
    "parentTable" => "workers",
    "childTable" => "address",
    "parentTable_data" => [
        [
            "column" => "firstname",
            "value" => "John",
            "constraint" => "not null"
        ],
        [
            "column" => "lastname",
            "value" => "Doe",
            "constraint" => "not null"
        ],
        [
            "column" => "phonenumber",
            "value" => null,
            "constraint" => "null"
        ],
        [
            "column" => "email",
            "value" => null,
            "constraint" => "null"
        ]
    ],
    "childTable_data" => [
        [
            "column" => "country",
            "value" => null,
            "constraint" => "not null"
        ],
        [
            "column" => "city",
            "value" => null,
            "constraint" => "not null"
        ],
        [
            "column" => "address",
            "value" => null,
            "constraint" => "null"
        ]
    ]
];

**One-to-Many Relationship:**
```php
// PHP Example
$requestPayload = [
    "relationship" => "oneToMany",
    "parentTableOne" => "department",
    "parentTableTwo" => "employees",
    "parentTableOne_data" => [
        [
            "column" => "name",
            "value" => "Sales Department",
            "constraint" => "not null"
        ]
    ],
    "parentTableTwo_data" => [
        [
            "column" => "employee_name",
            "value" => "John Doe",
            "constraint" => "not null"
        ],
        [
            "column" => "employee_name",
            "value" => "Jane Smith",
            "constraint" => "not null"
        ]
    ]
];

// JSON Example
{
  "relationship": "oneToMany",
  "parentTableOne": "department",
  "parentTableTwo": "employees",
  "parentTableOne_data": [
    {
      "column": "name",
      "value": "Sales Department",
      "constraint": "not null"
    }
  ],
  "parentTableTwo_data": [
    {
      "column": "employee_name",
      "value": "John Doe",
      "constraint": "not null"
    },
    {
      "column": "employee_name",
      "value": "Jane Smith",
      "constraint": "not null"
    }
  ]
}
```

**Many-to-Many Relationship:**
for elaready existing Records;
Instructions:

Replace <Table1_Name> with the name of the first parent table.
Replace <Table2_Name> with the name of the second parent table.
Replace <PrimaryKey_Value_Table1> with the primary key value of the record in the first parent table.
Replace <PrimaryKey_Value_Table2> with the primary key value of the record in the second parent table.
The simplified JSON format allows you to insert new relationships between existing records in the parent tables into the junction table. The "parentTableOne_pk" and "parentTableTwo_pk" fields specify the primary key values of the associated records in the respective parent tables.
{
    "relationship": "manyToMany",
    "parentTableOne": "<Table1_Name>",
    "parentTableTwo": "<Table2_Name>",
    "parentTableOne_pk": "<PrimaryKey_Value_Table1>",
    "parentTableTwo_pk": "<PrimaryKey_Value_Table2>"
}
Sure! Here's the PHP format for inserting records in a many-to-many relationship:

```php
// Assuming you have the JSON data stored in a variable called $jsonString
$jsonString = '{
    "relationship": "manyToMany",
    "parentTableOne": "<Table1_Name>",
    "parentTableTwo": "<Table2_Name>",
    "parentTableOne_pk": "<PrimaryKey_Value_Table1>",
    "parentTableTwo_pk": "<PrimaryKey_Value_Table2>"
}';

// Convert JSON to PHP associative array
$data = json_decode($jsonString, true);

// Assuming you have a function to insert records into the junction table, you can use it like this
if ($data['relationship'] === 'manyToMany') {
    $table1_name = $data['parentTableOne'];
    $table2_name = $data['parentTableTwo'];
    $pk_table1 = $data['parentTableOne_pk'];
    $pk_table2 = $data['parentTableTwo_pk'];

    // Your function to insert records in the junction table
    // Replace this with the actual function that inserts records
    insertRecordsInJunctionTable($table1_name, $table2_name, $pk_table1, $pk_table2);
}
```

In this PHP code, we first convert the JSON data into a PHP associative array using `json_decode`. Then, we extract the necessary values from the array, such as the table names and primary key values of the associated records in the parent tables.

Finally, we call a function named `insertRecordsInJunctionTable` (replace it with the actual function name you have for inserting records in the junction table). Inside this function, you can perform the actual insertion of records into the junction table using the provided table names and primary key values.

Note: The PHP code assumes that you have a function (`insertRecordsInJunctionTable`) or a method that handles the insertion of records into the junction table based on the provided parameters. You need to replace `insertRecordsInJunctionTable` with the actual implementation of your database insertion logic for the many-to-many relationship.

```php
// PHP Example
$requestPayload = [
    "relationship" => "manyToMany",
    "parentTableOne" => "students",
    "parentTableTwo" => "courses",
    "parentTableOne_data" => [
        [
            "column" => "name",
            "value" => "John Doe",
            "constraint" => "not null"
        ]
    ],
    "parentTableTwo_data" => [
        [
            "column": "course_name",
            "value": "Math",
            "constraint": "not null"
        ],
        [
            "column": "course_name",
            "value": "Science",
            "constraint": "not null"
        ]
    ]
};

// JSON Example
{
  "relationship": "manyToMany",
  "parentTableOne": "students",
  "parentTableTwo": "courses",
  "parentTableOne_data": [
    {
      "column": "name",
      "value": "John Doe",
      "constraint": "not null"
    }
  ],
  "parentTableTwo_data": [
    {
      "column": "course_name",
      "value": "Math",
      "constraint": "not null"
    },
    {
      "column": "course_name",
      "value": "Science",
      "constraint": "not null"
    }
  ]
}
```
{
  "relationship": "none",
  "table": "workers",
  "isManyRecords": "true",
  "isManyInsert": "true",
  "meta_data": [
    [
      {
        "column": "firstname",
        "value": "hope",
        "constraint": "not null"
      },
      {
        "column": "lastname",
        "value": "Bolithio",
        "constraint": "not null"
      },
      {
        "column": "phonenumber",
        "value": "+2560708059274",
        "constraint": "null"
      },
      {
        "column": "email",
        "value": "nicosjus@gmail.com",
        "constraint": "null"
      }
    ],
    [
      {
        "column": "firstname",
        "value": "hope",
        "constraint": "not null"
      },
      {
        "column": "lastname",
        "value": "Bolithio",
        "constraint": "not null"
      },
      {
        "column": "phonenumber",
        "value": "+2560708059274",
        "constraint": "null"
      },
      {
        "column": "email",
        "value": "nicosjus@gmail.com",
        "constraint": "null"
      },
      {
        "column": "firstname",
        "value": "hope",
        "constraint": "not null"
      },
      {
        "column": "lastname",
        "value": "Bolithio",
        "constraint": "not null"
      },
      {
        "column": "phonenumber",
        "value": "+2560708059274",
        "constraint": "null"
      },
      {
        "column": "email",
        "value": "nicosjus@gmail.com",
        "constraint": "null"
      }
    ]
  ]
}

These examples reflect the updated format without the "relationshipTable_data" field.
// PHP Example
$requestPayload = [
    "relationship" => "none",
    "tableName" => "courses",
    "tableData" => [
        [
            "column" => "course_id",
            "value" => null,
            "constraint" => "not null"
        ],
        [
            "column" => "course_name",
            "value" => "Math",
            "constraint" => "not null"
        ],
        [
            "column" => "course_description",
            "value" => "An introductory course to mathematics",
            "constraint" => "not null"
        ]
    ]
];

// JSON Example
{
  "relationship": "none",
  "table": "courses",
  "table_data": [
    {
      "column": "course_id",
      "value": null,
      "constraint": "not null"
    },
    {
      "column": "course_name",
      "value": "Math",
      "constraint": "not null"
    },
    {
      "column": "course_description",
      "value": "An introductory course to mathematics",
      "constraint": "not null"
    }
  ]
}


Title: Condition Builder Documentation

Introduction:
The condition builder is a utility that helps in constructing complex conditions for SQL queries. It provides a flexible way to define multiple conditions with various operators and combine them using logical operators.

Usage:
The condition builder accepts an array of conditions and generates the corresponding SQL condition statement.

Syntax:
The conditions array should be structured as follows:

php
Copy code
$conditions = [
    // Array of conditions
    [
        // Array of individual conditions
        [
            "column" => "column_name",
            "value" => "condition_value",
            "operator" => "comparison_operator",
        ],
        // Operator for combining multiple conditions (optional)
        "operator" => "logical_operator",
        // Combine operator for nested conditions (optional)
        "combineOperator" => "logical_operator",
        // Nested conditions (optional)
        [
            // Nested condition
            [
                "column" => "column_name",
                "value" => "condition_value",
                "operator" => "comparison_operator",
            ],
            // Additional nested conditions...
        ]
    ],
    // Additional conditions...
];
Parameters:

column: The name of the column to apply the condition on.
value: The value to compare with the column.
operator: The comparison operator to use. (e.g., "eq" for equals, "gt" for greater than, etc.)
operator (optional): The logical operator to combine multiple conditions within the same level. (e.g., "and" or "or")
combineOperator (optional): The logical operator to combine nested conditions within the same level. (e.g., "and" or "or")
Example:

php
Copy code
$conditions = [
    [
        [
            "column" => "year",
            "value" => 2023,
            "operator" => "eq",
        ],
        "operator" => "and",
        [
            "column" => "month",
            "value" => "June",
            "operator" => "eq",
        ],
        "combineOperator" => "or",
        [
            "column" => "name",
            "value" => "neco",
            "operator" => "eq",
        ]
    ]
];
The above example represents the following SQL condition:

sql
Copy code
(year = 2023 AND month = 'June') OR name = 'neco'
Conclusion:
The condition builder provides a convenient way to define complex conditions using an array-based structure. It allows you to construct SQL conditions with different operators and logical combinations, making it easier to build dynamic queries with varying conditions.
