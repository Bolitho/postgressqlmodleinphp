<?php
require_once('PostgresDataConnection.php');
require_once 'operators.php';

class DatabaseFetcher extends PostgresDataConnection
{
   private $connection;
   private $tableName;
   private $data;
   private $tables;
   private $conditions;
   private $isConditionSet;
   private $relatedTableNames = [];
   private $isParentTable = false;
   private $parentTable = "";
   private $hasManyToMany = false;
   private $junction_table="";

   public function __construct($tableName, $data)
   {
       if (empty($tableName)) {
           throw new Exception("Table name is required");
       }
       $this->data = $data;
       $this->conditions = isset($this->data["conditions"]) ? $this->data["conditions"] : [];
       $this->isConditionSet = isset($this->data["conditions"]);
       $this->connection = $this->getInstance()->getConnection();
       $this->tableName = $tableName;
       $hasManyToMany = isset($data["isManyToMany"]) ? $data["isManyToMany"] : false;
       $this->tables = $this->getTableNames($this->tableName, $hasManyToMany, $this->isConditionSet);
       $this->selectColumns();
       $this->joins();
       echo json_encode(["related_tables" => $this->tables]);
       echo "\n";

       if ($this->hasRelationship($this->tableName)) {
           $this->hasManyToMany = $this->isManyToMany();
           foreach ($this->tables as $key => $table) {
             if ($this->hasManyToMany) {
               $this->junction_table = $table["child_table"];
             }
               if ($hasManyToMany && ($table["child_table"] === $this->tableName)) {

                   $this->parentTable = $table["parent_table"];
                   $this->tableName = $table["parent_table"];
               } else {
                   $this->parentTable = $this->tableName;
               }
               if ($table["parent_table"] !== $this->tableName) {
                   $this->relatedTableNames[] = $table["parent_table"];
               }
               if (($table["child_table"] !== $this->tableName) && !$hasManyToMany) {
                   $this->relatedTableNames[] = $table["child_table"];
               }
           }
           echo json_encode(["table names" => $this->relatedTableNames]);
       }
   }

   protected function isManyToMany()
   {
       foreach ($this->tables as $key => $value) {
           if (strpos($value['constraint_name'], 'manytomany') !== false) {
               return true;
           }
       }
       return false;
   }

   private function selectColumns()
   {
       $select = "";
       $uniqueTables = [];
       echo json_encode(["tables" => $this->tables]);

       foreach ($this->tables as $value) {
           $childTable = $value["child_table"];
           $parentTable = $value["parent_table"];

           if (!isset($uniqueTables[$childTable])) {
               $select .= "{$childTable}.*,";
               $uniqueTables[$childTable] = true;
           }

           if (!isset($uniqueTables[$parentTable])) {
               $select .= "{$parentTable}.*,";
               $uniqueTables[$parentTable] = true;
           }
       }

       $select = rtrim($select, ",");
       //echo json_encode(["select" => $select]);
       return $select;
   }

   private function joins()
   {
       $joinConditions = [];

       foreach ($this->tables as $value) {
           $constraintName = $value['constraint_name'];
           $parentTable = $value['parent_table'];
           $childTable =  $value['child_table'];
           $joinType = '';
           $joinCondition = "{$childTable} ON {$parentTable}.{$parentTable}_id = {$childTable}.{$parentTable}_id";

           $data = [];

           if (strpos($constraintName, 'onetoone') !== false) {
               $joinType = ' INNER JOIN';

               $data = [
                   "joinType" => $joinType,
                   "joinCondition" => $joinType." ".$joinCondition,
                   "parentTable" => $parentTable,
                   "childTable" => $childTable
               ];
           } elseif (strpos($constraintName, 'onetomany') !== false) {
               $joinType = ' LEFT JOIN';

               $data = [
                   "joinType" => $joinType,
                   "joinCondition" => $joinType." ".$joinCondition,
                   "parentTable" => $parentTable,
                   "childTable" => $childTable
               ];
           } elseif (strpos($constraintName, 'manytomany') !== false) {
               $joinType = ' INNER JOIN';

               $data = [
                   "joinType" => $joinType,
                   "otherParentTable" => $value['other_parent_table'] ?? null,
                   "isManyToMany" => true,
                   "joinCondition" => $joinType." ".$joinCondition,
                   "parentTable" => $parentTable,
                   "childTable" => $childTable
               ];
           }

           $joinConditions[] = $data;
       }

       return $joinConditions;
   }

   public function findInfoOne($isAll = false)
   {
       $table_data = [];

       try {
           // Fetch one record based on conditions
           $whereClause = '';

           if ($this->hasRelationship($this->tableName)) {
               $conditionResult = null;
               $whereClause = '';

               if ($this->isConditionSet) {
                   $conditionResult = $this->buildPreparedCondition($this->conditions, $this->tableName);
                   $whereClause = $conditionResult['condition'];
               }

               $parentTable = '';

               $select = "SELECT ";
               $limit = " LIMIT 1";

               $joinConditions = $this->joins();

               foreach ($joinConditions as $data) {
                   if ($this->isConditionSet && isset($data["isManyToMany"])) {
                       $parentTable = $data["parentTable"];
                   }

                   $sql = $select . $this->renameAndSetColumnNames($data["parentTable"]);
                   $sql .= ", " . $this->renameAndSetColumnNames($data["childTable"]);
                   $sql .= " FROM {$data["parentTable"]}";
                   $sql .= $data["joinCondition"];

                   if (!empty($whereClause) && ($this->tableName === $data["parentTable"] || $this->tableName === $data["childTable"])) {
                       $sql .= " WHERE " . $whereClause;
                   }

                   if (!$isAll) {
                       echo json_encode(["return one" => 1]);
                       echo "\n";
                       $sql .= $limit;
                   }

                   $statement = $this->connection->prepare($sql);

                   if (!empty($conditionResult['params'])) {
                       foreach ($conditionResult['params'] as $paramName => $paramValue) {
                           $statement->bindValue($paramName, $paramValue);
                       }
                   }

                   $statement->execute();
                   $results = $statement->fetchAll(PDO::FETCH_ASSOC);

                   if ($results) {
                       foreach ($results as $key => $value) {
                           if (!isset($data['isManyToMany'])) {
                               $table_data["resultsTable"][$key] = $value;
                           } else {
                               $table_data[$data['parentTable']][$key] = $value;
                           }

                           if (isset($data["isManyToMany"]) && $this->isConditionSet) {
                               $subsql = "SELECT " . $this->renameAndSetColumnNames($data["otherParentTable"]) . " FROM {$data["otherParentTable"]} INNER JOIN {$data["childTable"]} ON {$data["otherParentTable"]}.{$data["otherParentTable"]}_id = {$data["childTable"]}.{$data["otherParentTable"]}_id WHERE {$data["childTable"]}.{$data["parentTable"]}_id = ?";
                               $substatement = $this->connection->prepare($subsql);

                               $substatement->bindValue(1, $value[$parentTable."_".$parentTable . "_id"]);
                               $substatement->execute();
                               $subresults = $substatement->fetchAll(PDO::FETCH_ASSOC);

                               if ($subresults) {
                                   foreach ($subresults as $subkey => $subvalue) {
                                       $table_data[$data["otherParentTable"]][$subkey] = $subvalue;
                                   }
                               }
                           }
                       }
                   }
               }

               return $table_data;
           }

           return [];
       } catch (PDOException $e) {
           throw new Exception("Error occurred: " . $e->getMessage());
       }
   }

   public function findAll()
   {
       $conditionResult = null;
       $tableData = [];
       $whereClause = "";

       if ($this->isConditionSet) {
           $conditionResult = $this->buildPreparedCondition($this->conditions, $this->tableName);
           $whereClause = $conditionResult['condition'];
       }

       try {
           $select = "SELECT * ";
           $sql = $select . " FROM $this->tableName";

           if (!empty($whereClause) && $this->isConditionSet) {
               $sql .= " WHERE " . $whereClause;
           }

           $statement = $this->connection->prepare($sql);

           if (!empty($conditionResult['params'])) {
               foreach ($conditionResult['params'] as $paramName => $paramValue) {
                   $statement->bindValue($paramName, $paramValue);
               }
           }

           $statement->execute();
           $results = $statement->fetchAll(PDO::FETCH_ASSOC);

           if (!empty($results)) {
               foreach ($this->relatedTableNames as $relatedTableName) {
                   $subsql = "SELECT * ";
                   $subsql .= "FROM $relatedTableName";
                   $subsql .= " WHERE " . $this->parentTable . "_id IN (SELECT " . $relatedTableName . "_id FROM {$this->junction_table} WHERE " . $this->tableName . "_id = ?)";

                   $substatement = $this->connection->prepare($subsql);
                   echo json_encode(["sql"=>$substatement]);
                   echo "\n";

                   foreach ($results as $key => $data) {
                       $substatement->execute([$data[$this->tableName . "_id"]]);
                       $resultData = $substatement->fetchAll(PDO::FETCH_ASSOC);

                       if ($resultData) {
                           $results[$key][$relatedTableName] = $resultData;
                       }
                   }
               }
           }

           return $results;
       } catch (\Exception $e) {
           throw new \Exception("Error: " . $e->getMessage());
       }
   }
}
?>
