<?php class TableNameRequiredException extends Exception
{
    public function __construct()
    {
        parent::__construct("Table name is required", 0, null);
    }
} ?>
