<?php
require_once('PostgresDataConnection.php');
require_once 'operators.php';

class DatabaseUpdater extends PostgresDataConnection
{
    private $connection;
    private $tableName;
    private $conditions = [];
    private $data;
    private $multiple_data =[]; // Fixed property name
    private $multiRowMultiValueUpdate;
    private $isManyToManyRelationship = false;
    private $columns = [];
    private $columnNameData = [];
    private $columnNames = [];
    private $table_id ="";
    private $updateIsToDoneFromParent;


    public function __construct($settings)
    {  //echo json_encode(["settings"=>$settings]);
        // Extract settings
        $this->tableName = $settings['table'];
        $this->table_id = "{$this->tableName}_id";
        $this->columnNames = $this->getColumnNames($this->tableName);
       $this->updateIsToDoneFromParent= $settings["updateIsToDoneFromParent"]??false;
       $this->multiRowMultiValueUpdate = $settings['multiRowMultiValueUpdate'] ?? false;

        if ($this->multiRowMultiValueUpdate) {
            $this->multiple_data = $settings['data'];

        } else {
            $this->conditions = $settings['conditions'];
            $this->data = $settings['data'];


            // Check for missing table name
            if ($this->tableName === null) {
                throw new Exception("Error: The table name is required");
            }

            // Check for missing conditions or data for single row update
            if (empty($this->conditions) || $this->data === null) {
                throw new \Exception("Conditions and data to be updated are required");
            }
        }


         $table_meta_data = $this->getOneTable($this->tableName);
         if (!empty($this->multiple_data)) {
            $this->data = $this->multiple_data["data"][0];
             $this->conditions = $this->multiple_data["conditions"][0];
         }
          $isOneToMany =  strpos($table_meta_data["constraint_name"], 'onetomany') !== false;
          $isManyToMany = strpos($table_meta_data["constraint_name"], 'manytomany') !== false;

          if ($isOneToMany) {
            $this->checkOneToManyRelationship($table_meta_data);
          }
          if ($isManyToMany) {
            $this->checkManyToManyRelationship($table_meta_data);
          }




        // Get the database connection instance
        $this->connection = $this->getInstance()->getConnection(); // Make sure to implement this method correctly
    }
    private function checkOneToManyRelationship($table_meta_data)
 {
     if ($table_meta_data !== null) {

         if (!$this->validateTableColumns()) {
             $this->handleTableValidationError();
         }
         $isTrue =false;
          $requiredKeys = ["{$table_meta_data['child_table']}_id","{$table_meta_data['parent_table']}_id"];
         foreach ($requiredKeys as $key) {
           foreach ($this->conditions as $condition) {
              if (isset($condition['column']) && $condition['column'] === $key) {
                $isTrue = true;
                break;
      }
  }
}
if (!$isTrue) {
               $this->handleConditionalErrors();
           }

     }
 }

 private function checkManyToManyRelationship($table_meta_data)
 {
     if ($table_meta_data !== null) {

         $replacement = str_replace("students", "", $table_meta_data['child_table']);
         $otherParentTable = trim($replacement, '_');
         $this->isManyToManyRelationship = true;

       if (!$this->updateIsToDoneFromParent) {
         $this->tableName = $table_meta_data['child_table'];
       }



         if ($this->validateTableColumns()) {
           $isTrue =false;
            $requiredKeys = ["{$otherParentTable}_id","{$table_meta_data['parent_table']}_id"];
           foreach ($requiredKeys as $key) {
             foreach ($this->conditions as $condition) {
                if (isset($condition['column']) && $condition['column'] === $key) {
                  $isTrue = true;
                  break;
        }
    }
}



          if (!$isTrue) {
                 $this->handleConditionalErrors();
             }
         } else {
             $this->tableName = $table_meta_data['parent_table'];
             if (!$this->validateTableColumns()) {
                 $this->handleTableValidationError();
             }
         }
     }
 }



    private function handleConditionalErrors()
{
    $errorMessage = "Both  primary keys parentTable and childTable are required
    for updating a record in the child table in a one-to-many relationship and
    for updating a record in the many-to-many junction table.
    Both the primary keys of the two parent tables are required.";

    echo json_encode(["Error" => $errorMessage]);
    exit(); // Add exit() to terminate the script after displaying the error message
}


    private function validateTableColumns()
    {
        $this->columnNames = $this->getColumnNames($this->tableName);
        if (!empty($this->columnNames)) {
            if ($this->columnValidateForConditions() && $this->columnValidateForData()) {
              return true;
            } else {
              return false;
            }
        }
    }
     private  function  handleTableValidationSuccess()
     {
        echo json_encode(["table_success" => "This is the right table. All columns are valid for both conditions and data."]);
     }
    private function handleTableValidationError()
    {
        echo json_encode(["table_error" => "The table provided does not contain all the columns you passed in both the conditions and the data. Please provide a correct table name or check the column names of your conditions and data"]);
        exit();
    }

    public function updateOne()
    {
        $conditionResult = $this->buildPreparedCondition($this->conditions, $this->tableName);
        $whereClause = "WHERE " . $conditionResult['condition'];

        try {
            // Use a subquery to select the ID that matches the condition
            $subQuery = "SELECT {$this->tableName}_id FROM {$this->tableName} {$whereClause} LIMIT 1";

            $subStatement = $this->connection->prepare($subQuery);
            // Bind the condition parameters
            foreach ($conditionResult['params'] as $paramName => $paramValue) {
                $subStatement->bindValue($paramName, $paramValue);
            }

            // Execute the subquery
            $subStatement->execute();

            // Get the selected ID directly
            $selectedId = $subStatement->fetch(PDO::FETCH_COLUMN);

            if (!$selectedId) {
                throw new Exception("No record found matching the condition.");
            }

            // Prepare the UPDATE query
            $placeholders = $this->prepareUpdateQuery();

            $updateQuery = "UPDATE {$this->tableName} SET {$placeholders['columns']} WHERE {$this->tableName}_id = ? RETURNING {$this->tableName}_id";
            $updateStatement = $this->connection->prepare($updateQuery);

            // Merge the values with the selected ID
            $values = array_merge($placeholders["values"], [$selectedId]);

            // Execute the UPDATE query
            $updateStatement->execute($values);

            // Return the updated primary ID
            $updatedPrimaryId = $updateStatement->fetchColumn();


            return  $this->isManyToManyRelationship?
                   $this->returnIdsOfJunctionTable($updatedPrimaryId):
                   ["{$this->tableName}_id"=>$updatedPrimaryId];
        } catch (PDOException $e) {
            throw new Exception("Update error: " . $e->getMessage());
        }
    }
    protected function returnIdsOfJunctionTable($junctionTableId)
    {
        $sql = "SELECT {$this->table_id} FROM {$this->tableName} WHERE {$this->tableName}_id=?";
        $statement = $this->connection->prepare($sql);
        $statement->execute([$junctionTableId]);
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateMany()
 {
     $ids = [];

     try {

         if (empty($this->multiple_data) || !is_array($this->multiple_data)) {
             throw new Exception("Data to update is required and should be an array.");
         }

         if (!isset($this->multiple_data['conditions']) || !isset($this->multiple_data['data'])) {
             throw new Exception("The 'meta_data' array should have both 'conditions' and 'data' arrays.");
         }

         $conditionGroups = $this->multiple_data['conditions'];
         $dataGroups = $this->multiple_data['data'];

         if (count($conditionGroups) !== count($dataGroups)) {
             throw new Exception("The number of condition groups should be equal to the number of data groups.");
         }

         for ($i = 0; $i < count($conditionGroups); $i++) {
             $data = [
                 'conditions' => $conditionGroups[$i],
                 'data' => $dataGroups[$i]
             ];

             $affectedRows = $this->performUpdate($data);
             $ids = array_merge($ids, $affectedRows);
         }

         return $ids;
     } catch (PDOException $e) {
         throw new Exception("Error occurred when updating many records: " . $e->getMessage());
     }
 }



    private function performUpdate($record)
    {
        if (!isset($record['data']) || !is_array($record['data'])) {
            throw new Exception("Each record in the data array must have a 'values' key containing an array of column-value pairs.");
        }

        // Input validation for conditions can be added here.

        $this->data = $record['data'];
        $this->conditions = $record['conditions'];

        $placeholders = $this->prepareUpdateQuery();

        // Add conditions to the WHERE clause
        $whereClause = '';
        $ConditionsValues = [];
        if (!empty($this->conditions)) {
            $conditionResult = $this->processedConditions($this->conditions);
            $whereClause = "WHERE " . $conditionResult['condition'];
            $ConditionsValues = $conditionResult['params'];
        }

        $query = "UPDATE {$this->tableName} SET {$placeholders["columns"]} {$whereClause} RETURNING {$this->tableName}_id";

        // Prepare the statement and use a transaction for the update
        try {
            $this->connection->beginTransaction();

            $statement = $this->connection->prepare($query);
            $statement->execute(array_merge($placeholders["values"], $ConditionsValues));

            $ids = $statement->fetchAll(PDO::FETCH_ASSOC);

            $this->connection->commit();
            return $ids;
        } catch (Exception $e) {
            // Handle the exception, and rollback the transaction on failure.
            $this->connection->rollBack();
            throw $e;
        }
    }

    private function prepareUpdateQuery()
    {
        if (isset($this->data[$this->tableName . "_id"])) {
            unset($this->data[$this->tableName . "_id"]);
        } // Remove the 'id' field from the data array

        $updates = [];
        $values = [];
       

        foreach ($this->data as $key => $value) {
            $updates[] = $value["column"] . "=?";

            $values[] = $value["value"];
        }

        $columns = implode(", ", $updates);

        return [
            'columns' => $columns,
            'values' => $values
        ];
    }

    protected function extractColumns()
    {
        foreach ($this->conditions as $key => $value) {
            $this->columns[] = $value['column'];
        }
    }

    protected function columnValidateForConditions()
    {
        $columnNamesConditions = [];
        foreach ($this->conditions as $key => $value) {
            $columnNamesConditions[] = $value['column'];
        }
        foreach ($columnNamesConditions as $value) {
            if (!in_array($value, $this->columnNames)) {
                return false;
            }
        }
        return true;
    }

    protected function columnValidateForData()
    {
        $columnNamesData = [];

        foreach ($this->data as $key => $value) {

            $columnNamesData[] = $value['column'];
        }
        foreach ($columnNamesData as $value) {
            if (!in_array($value, $this->columnNames)) {
                return false;
            }
        }
        return true;
    }
}
?>
