<?php

require_once 'PostgresDataConnection.php';
require_once 'operators.php';

class Computations extends PostgresDataConnection
{
    private $databaseConnection;
    private $settings;
    private $database;

    public function __construct($settings = [])
    {
        $this->database = $this->getInstance();
        $this->databaseConnection = $this->database->getConnection();
        $this->settings = $settings;
    }

    public function balance($data)
    {
        $is_positive = $data["is_positive"] ?? false;
        $value = $data['value_to_subtract'] ?? 0;
        $conditions = $data['conditions'] ?? [];
        $column = $data["column"] ?? "";
        $tableName = $data["tableName"] ?? "";

        $conditionsValues = [];
        $condition = "";

        if (!empty($conditions)) {
            // Assuming "processConditions" method is defined in "operators.php"
            $processedConditions = $this->processedConditions($conditions);
            $condition = $processedConditions['condition'];
            $conditionsValues = $processedConditions['params'];
        }

        try {
            $data = $this->processedData($condition, $conditionsValues, $tableName, $column);
            $total = $this->sumCalculations($data);

            if ($is_positive) {
                return abs($total["total"] - $value);
            }

            return $total['total'] - $value;
        } catch (PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    protected function balanceBetweenSumOfColumnsInDifferentTables($data)
    {
        $firstTable = $data['firstTable'] ?? "";
        $firstConditions = $data["firstConditions"]["conditions"] ?? [];
        $secondsConditions = $data['secondsConditions']["conditions"] ?? [];
        $secondTable = $data['secondTable'] ?? "";
        $firstColumn = $data['firstColumn']["column"] ?? "";
        $secondColumn = $data['secondColumn']["column"] ?? "";
        $is_positive = $data["is_positive"] ?? false;
        $firstCondition = "";
        $firstConditionValues = [];
        $secondCondition = "";
        $secondConditionValues = [];

        if (!empty($firstConditions)) {
            $firstProcessedConditions = $this->processedConditions($firstConditions);
            $firstCondition = $firstProcessedConditions['condition'];
            $firstConditionValues = $firstProcessedConditions["params"];
        }

        if (!empty($secondsConditions)) {
            $secondProcessedConditions = $this->processedConditions($secondsConditions);
            $secondCondition = $secondProcessedConditions['condition'];
            $secondConditionValues = $secondProcessedConditions["params"];
        }

        $firstData = $this->processedData($firstCondition, $firstConditionValues, $firstTable,$firstColumn);
        try {
            $firstTotal = $this->sumCalculations($firstData);
            $secondData = $this->processedData($secondCondition, $secondConditionValues, $secondTable, $secondColumn);
            $secondTotal = $this->sumCalculations($secondData);

            if ($is_positive) {
                return abs($firstTotal['total'] - $secondTotal["total"]);
            }


            return abs($firstTotal["total"] - $secondTotal["total"]);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function sumCalculations($data): array
    {
        $column = $data["column"] ?? "";
        $tableName = $data["tableName"] ?? "";
        $condition = $data["condition"] ?? "";
        $conditionsValues = $data["conditionsValues"] ?? [];

        try {
            $sql = "SELECT SUM($column) AS total FROM $tableName";

            if (!empty($condition)) {
                $sql .= " WHERE $condition";
            }

            $statement = $this->databaseConnection->prepare($sql);

            if (!empty($conditionsValues)) {
                $statement->execute($conditionsValues);
            } else {
                $statement->execute();
            }

            return $statement->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function processedData($condition, $conditionsValues, $tableName, $column)
    {
        $data = [];
        $data["condition"] = $condition;
        $data["conditionsValues"] = $conditionsValues;
        $data["tableName"] = $tableName;
        $data['column'] = $column;
        return $data;
    }
    public function sum($data)
    {
      return sumCalculations($data);
    }

    public function product($data)
    {
      
    }


}
?>
