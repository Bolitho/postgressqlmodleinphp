 <?php
require_once 'PostgresDataConnection.php';
require_once 'operators.php';

class DatabaseFetcher extends PostgresDataConnection
{
    private $connection;
    private $tableName;
    private $data;
    private $tables;
    private $conditions;
    private $isConditionSet;
    private $relatedTableNames = [];
    private $isParentTable = false;
    private $parentTable = "";
    private $junction_data = [];
    private $junction_table = "";
    private $hasManyToMany = false;
    private $manyToManyTableNames;
    private $parentManyToManyTable = "";
    private $has_no_relationship;
    private $cursorName = 'my_cursor';
    private $batchSize = 100;

    public function __construct($tableName, $data)
    {
        if (empty($tableName)) {
            throw new Exception("Table name is required");
        }
        $this->data = $data;
        $this->conditions = isset($this->data["conditions"]) ? $this->data["conditions"] : [];
        $this->isConditionSet = isset($this->data["conditions"]);
        $this->connection = $this->getInstance()->getConnection();
        $this->tableName = $tableName;
        $this->has_no_relationship = $this->hasRelationship($this->tableName);
        $hasManyToMany = isset($data["isManyToMany"]) ? $data["isManyToMany"] : false;
        $this->tables = $this->getTableNames($this->tableName, $hasManyToMany, $this->isConditionSet);
        echo json_encode(["tables" => $this->tables]);
        echo "\n";

        if ($this->hasRelationship($this->tableName)) {
            $this->hasManyToMany = $this->isManyToMany();

            foreach ($this->tables as $key => $table) {
                if ($this->hasManyToMany) {
                    $this->junction_table = $table["child_table"];
                    $this->manyToManyTableNames = explode("_", $this->junction_table);
                    if ($hasManyToMany && ($table["child_table"] === $this->tableName)) {
                        $this->parentTable = $table["parent_table"];
                        $this->parentManyToManyTable = trim(str_replace($table["parent_table"] . "_", '', $this->tableName), "_");
                    }
                    $this->parentManyToManyTable = trim(str_replace($this->tableName, '', $this->junction_table), "_");

                }

                if ($table["parent_table"] === $this->tableName) {
                    $this->isParentTable = true;
                    $this->parentTable = $this->tableName;
                    if ($table["parent_table"] !== $this->tableName) {
                        $this->parentTable = $this->tableName;
                    }
                } else {
                    $this->parentTable = $table["parent_table"];
                }
                if ($table["parent_table"] !== $this->tableName) {
                    $this->relatedTableNames[] = $table["parent_table"];
                }
                  echo json_encode(["hasManyToMany"=>$this->hasManyToMany]);
                if (($table["child_table"] !== $this->tableName) && !$this->hasManyToMany) {

                   exit();
                    $this->relatedTableNames[] = $table["child_table"];
                }
            }
            echo json_encode(["table names" => $this->relatedTableNames]);
        }
    }

    protected function isManyToMany()
    {
        foreach ($this->tables as $key => $value) {
            if (strpos($value['constraint_name'], 'manytomany') !== false) {
                return true;
            }
        }
        return false;
    }

    private function selectColumns()
    {
        $select = "";
        $uniqueTables = [];
        echo json_encode(["tables" => $this->tables]);

        foreach ($this->tables as $value) {
            $childTable = $value["child_table"];
            $parentTable = $value["parent_table"];

            if (!isset($uniqueTables[$childTable])) {
                $select .= "{$childTable}.*,";
                $uniqueTables[$childTable] = true;
            }

            if (!isset($uniqueTables[$parentTable])) {
                $select .= "{$parentTable}.*,";
                $uniqueTables[$parentTable] = true;
            }
        }

        $select = rtrim($select, ",");
        //echo json_encode(["select" => $select]);
        return $select;
    }

    private function joins()
    {
        $joinConditions = [];

        foreach ($this->tables as $value) {
            $constraintName = $value['constraint_name'];
            $parentTable = $value['parent_table'];
            $childTable =  $value['child_table'];
            $joinType = '';
            $joinCondition = "{$childTable} ON  {$parentTable}.{$parentTable}_id =  {$childTable}.{$parentTable}_id";

            $data = [];

            if (strpos($constraintName, 'onetoone') !== false) {
                $joinType = ' INNER JOIN';

                $data = [
                    "joinType" => $joinType,
                    "joinCondition" => $joinType . " " . $joinCondition,
                    "parentTable" => $parentTable,
                    "childTable" => $childTable
                ];
            } elseif (strpos($constraintName, 'onetomany') !== false) {
                $joinType = ' LEFT JOIN';

                $data = [
                    "joinType" => $joinType,
                    "joinCondition" => $joinType . " " . $joinCondition,
                    "parentTable" => $parentTable,
                    "childTable" => $childTable
                ];
            } elseif (strpos($constraintName, 'manytomany') !== false) {
                $joinType = ' INNER JOIN';

                $data = [
                    "joinType" => $joinType,
                    "otherParentTable" => $value['other_parent_table'] ?? null,
                    "isManyToMany" => true,
                    "joinCondition" => $joinType . " " . $joinCondition,
                    "parentTable" => $parentTable,
                    "childTable" => $childTable
                ];
            }

            $joinConditions[] = $data;
        }

        return $joinConditions;
    }

    public function findInfoOne()
    {
        try {
            $this->connection->beginTransaction();

            // Fetch one record based on conditions
            $whereClause = '';
            $parentTable = '';

            $select = "SELECT ";
            $limit = " LIMIT 1";

            $sql = $select . "* FROM $this->tableName";

            if ($this->isConditionSet) {
                $conditionResult = $this->buildPreparedCondition($this->conditions, $this->tableName);
                $whereClause = $conditionResult['condition'];
            }

            if (!empty($whereClause) && $this->isConditionSet) {
                $sql .= " WHERE " . $whereClause;
            }

            $sql .= $limit;

            $statement = $this->connection->prepare($sql);

            if (!empty($conditionResult['params'])) {
                foreach ($conditionResult['params'] as $paramName => $paramValue) {
                    $statement->bindValue($paramName, $paramValue);
                }
            }

            $statement->execute();
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);

            if (!$this->hasRelationship($this->tableName)) {
                $this->connection->commit();
                return $results;
            }

            if ($this->hasManyToMany) {
                $results = $this->processManyToManyResults($results);
            }

            $results = $this->processRelatedTableResults($results);

            $this->connection->commit();
            return $results;
        } catch (PDOException $e) {
            $this->connection->rollBack();
            throw new Exception("Error occurred: " . $e->getMessage());
        }
    }

    public function findAll()
    {
        if ($this->hasRelationship($this->tableName)) {

        }

        $conditionResult = null;
        $tableData = [];
        $whereClause = "";
        $conditionsValues = [];

        if ($this->isConditionSet) {
            $conditionResult = $this->processedConditions($this->conditions);
            $whereClause = $conditionResult['condition'];
            $conditionsValues = $conditionResult["params"];
        }

        try {
            $this->connection->beginTransaction();

            $select = "SELECT * ";
            $sql = $select . " FROM $this->tableName";

            if (!empty($whereClause) && $this->isConditionSet) {
                $sql .= " WHERE " . $whereClause;
            }

            $cursorSql = "DECLARE $this->cursorName CURSOR FOR $sql";
            $statement = $this->connection->prepare($cursorSql);
            if (!empty($conditionsValues)) {
                $statement->execute($conditionsValues);
            }else {
                 $statement->execute();
            }


            $results = [];

            while (true) {
                $fetchSql = "FETCH $this->batchSize FROM $this->cursorName";
                $fetchStatement = $this->connection->prepare($fetchSql);
                $fetchStatement->execute();
                $batchResults = $fetchStatement->fetchAll(PDO::FETCH_ASSOC);

                if (!$batchResults) {
                    break;
                }

                if (!$this->has_no_relationship) {
                    $results = array_merge($results, $batchResults);
                }

                if ($this->hasManyToMany) {
                    $batchResults = $this->processManyToManyResults($batchResults);
                }

                $batchResults = $this->processRelatedTableResults($batchResults);

                $results = array_merge($results, $batchResults);
            }

            $this->connection->commit();
            return $results;
        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new \Exception("Error: " . $e->getMessage());
        }
    }

    private function processManyToManyResults($results)
 {
     $subsql = "SELECT * ";
     $subsql .= "FROM " . $this->parentManyToManyTable;
     $subsql .= " WHERE " . $this->parentManyToManyTable . "_id = ?";
     $substatement = $this->connection->prepare($subsql);

     if (!empty($results)) {
         foreach ($results as $resultKey => $data) {
             $this->junctionData($data["{$this->parentTable}_id"]);

             $results[$resultKey][$this->parentManyToManyTable] = []; // Initialize the array to hold many-to-many data
             foreach ($this->junction_data as $value) {
                  $substatement->execute([$value[$this->parentManyToManyTable . "_id"]]);
                 $resultData = $substatement->fetchAll(PDO::FETCH_ASSOC);
                 $results[$resultKey][$this->parentManyToManyTable][] = $resultData;
             }
         }
     }

     return $results;
 }


    private function processRelatedTableResults($results)
    {
        if (!empty($results)) {
            $length = 0;
            while ($length < count($this->relatedTableNames)) {
                $subsql = "SELECT * ";
                $subsql .= "FROM " . ($this->hasManyToMany ? $this->parentManyToManyTable : $this->relatedTableNames[$length]);
                $subsql .= " WHERE " . ($this->hasManyToMany ? "{$this->parentManyToManyTable}_id" : $this->parentTable) . "_id = ?";

                $substatement = $this->connection->prepare($subsql);

                foreach ($results as $resultKey => $data) {
                    $substatement->execute([$data[$this->parentTable . "_id"]]);
                    $resultData = $substatement->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($resultData as $innerKey => $innerTableValue) {

                          $primarykey = $data["{$this->parentTable}_id"];
                          $foreignKey =  !$this->hasManyToMany?$innerTableValue["{$this->parentTable}_id"]:$innerTableValue["{$this->parentManyToManyTable}_id"];


                        if ( $primarykey===$foreignKey) {
                            if (!isset($results[$resultKey][!$this->hasManyToMany?$this->relatedTableNames[$length]:$this->parentManyToManyTable])) {
                                $results[$resultKey][!$this->hasManyToMany?$this->relatedTableNames[$length]:$this->parentManyToManyTable] = [];
                            }


                            $results[$resultKey][!$this->hasManyToMany
                            ?$this->relatedTableNames[$length]:$this->parentManyToManyTable]=
                            array_merge($results[$resultKey][!$this->hasManyToMany
                            ?$this->relatedTableNames[$length]:$this->parentManyToManyTable], $innerTableValue);
                        }
                    }
                }

                $length += 1;
            }
        }

        return $results;
    }

    public function getPrimaryKeyColumnName($tableName)
    {
        $sql = "SELECT a.attname
                FROM pg_index i
                JOIN pg_attribute a ON a.attrelid = i.indrelid
                WHERE i.indrelid = (
                    SELECT oid
                    FROM pg_class
                    WHERE relname = :tableName
                )
                AND i.indisprimary
                AND a.attnum = ANY(i.indkey)";

        $statement = $this->connection->prepare($sql);
        $statement->bindValue(':tableName', $tableName);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            return $result['attname'];
        }

        return null;
    }

    private function junctionData($idValue)
    {
        $primaryKeyName = $this->getPrimaryKeyColumnName($this->junction_table);

        $sql = "SELECT * FROM {$this->junction_table} WHERE  {$this->parentTable}_id=?";
        $statement = $this->connection->prepare($sql);
        $statement->execute([$idValue]);
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        foreach ($results as $value_results) {
            $tableNamesId = [];

            foreach ($this->manyToManyTableNames as $tableName) {
                $tableNamesId[] = $tableName . "_id";
            }

            $junctionDataItem = [];

            foreach ($value_results as $key => $value) {
                if ($key !== $primaryKeyName && in_array($key, $tableNamesId)) {
                    $junctionDataItem[$key] = $value;
                }
            }

            $this->junction_data[] = $junctionDataItem;
        }
    }
}

?>
