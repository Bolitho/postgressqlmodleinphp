<?php
require_once('PostgresDataConnection.php');
require_once 'operators.php';

class DatabaseInsertOperations extends PostgresDataConnection
{
    private $database;
    private $tableName;
    private $data;
    private $multipleData;
    private $isManyRecords = false;

    public function __construct($settings)
{


   if (!isset($settings['data'])) {
       throw new \Exception("The data to be inserted is needed.");
   }

   if (strtolower($settings["data"]["relationship"]) === "none") {
       if (!isset($settings["tableName"])) {
           throw new \Exception("The table name is required.");
       }
   }

   if (isset($settings['isManyRecords'])) {
       $this->isManyRecords = $settings['isManyRecords'];
   }

   $this->tableName = isset($settings["tableName"]) ? $settings["tableName"] : null;
   $this->data = $this->isManyRecords ? null : $settings['data'];
   $this->multipleData = $this->isManyRecords ? $settings['data'] : null;

   $this->database = $this->getInstance();
}


public function insertOne()
{
if (!isset($this->data['relationship'])) {
   throw new \Exception("No relationship key is set. Please provide a valid relationship value.");
}

$relationshipType = $this->getRelationshipType($this->data['relationship']);

if ($relationshipType !== false) {
   $this->handleRelationship($relationshipType, $this->data);

   return strtolower($this->data["relationship"]) === "none" ?
       $this->getLastInsertedId() : $this->lastInsertedIds();
} else {
   throw new \Exception("No relationship matches what you have provided. Please provide a valid relationship value.");
}
}
public function insertMany()
{
    $ids = [];
    try {
        $relationship = strtolower($this->multipleData["relationship"]);

        switch ($relationship) {
            case "none":
                $ids = $this->insertManyNone();
                break;
            case "manytomany":
                $ids = $this->insertManyToMany();
                break;
            default:
                $ids = $this->insertManyToOne();
                break;
        }

        return $ids;
    } catch (PDOException $e) {
        throw new Exception("Error inserting multiple records: " . $e->getMessage());
    }
}

private function insertManyNone()
{
   $ids = [];
   $tableName = $this->tableName;
   $backup = $this->multipleData["backup"] ?? false;
   $relationship = $this->multipleData["relationship"] ?? null;

   foreach ($this->multipleData["table_data"] as $data) {
       $this->data = [
           "relationship" => $relationship,
           "table_data" => $data,
           "table" => $tableName,
           "backup" => $backup
       ];

       $ids[] = $this->insertOne();
   }

   return $ids;
}


private function insertManyToMany()
{
    $ids = [];
    $parentTableOne = $this->multipleData['parentTableOne'];
    $parentTableTwo = $this->multipleData["parentTableTwo"];
    $parentTableOnekey = "parentTableOne_data";
    $parentTableTwokey = "parentTableTwo_data";
    // Assuming $parentTableOnekey and $parentTableTwokey are defined somewhere else in the code.

 if (!isset($this->multipleData["meta_data"][$parentTableOnekey]) && !isset($this->multipleData["meta_data"]["parentTableOne_pk"])) {
     throw new \Exception("Please provide a list of primary keys or the parent table data for parentTableOne");
 }

 if (!isset($this->multipleData["meta_data"][$parentTableTwokey]) && !isset($this->multipleData["meta_data"]["parentTableTwo_pk"])) {
     throw new \Exception("Please provide a list of primary keys or the parent table data for parentTableTwo");
 }

    // Use the null coalescing operator to set default values for keys
   $parentOneKey = isset($this->multipleData["meta_data"][$parentTableOnekey])
    ?$parentTableOnekey
    : "parentTableOne_pk";
    $parentTwoKey =isset($this->multipleData["meta_data"][$parentTableTwokey])
    ?$parentTableTwokey:
    "parentTableTwo_pk";


    $parentTableDataOne = $this->multipleData["meta_data"][$parentTableOnekey]??
                          $this->multipleData["meta_data"]["parentTableOne_pk"] ?? [];


    $parentTableDataTwo = $this->multipleData["meta_data"][$parentTableTwokey] ??
                          $this->multipleData["meta_data"]["parentTableTwo_pk"] ?? [];


    $backup = $this->multipleData['backup'] ?? false;
    $isCascade = $this->multipleData['cascade'] ?? false;

    $this->data = [
        "relationship" => $this->multipleData["relationship"],
        "parentTableOne" => $parentTableOne,
        "parentTableTwo" => $parentTableTwo,
        "backup" => $backup,
        "cascade" => $isCascade
    ];

    for ($i = 0; $i < count($parentTableDataOne); $i++) {
     $parentTableOneValue = $parentTableDataOne[$i];
     $parentTableTwoValue = $parentTableDataTwo[$i];
     $this->data[$parentOneKey] =  $parentTableOneValue;
     $this->data[$parentTwoKey] =  $parentTableTwoValue;
      $ids[] = $this->insertOne();}

    return $ids;
}




private function insertManyToOne()
{

  $parentTableKey = "parentTable_data";
  $childTableKey = "childTable_data";
  if (!isset($this->multipleData['meta_data'][$parentTableKey]) && !isset($this->multipleData['meta_data']["parentTable_pk"])) {
      throw new \Exception("Please provide either a list of primary keys or the parent table data.");
  }

  // Check if childTableData is provided
  if (!isset($this->multipleData['meta_data'][$childTableKey])) {
      throw new \Exception("Please provide the child table data.");
  }
   $ids = [];
   $relationship = $this->multipleData["relationship"];
   $parentTable = $this->multipleData['parentTable'];
   $childTable = $this->multipleData["childTable"];


   $key = isset($this->multipleData['meta_data'][$parentTableKey])?$parentTableKey : "parentTable_pk";
   $parentTableData = $this->multipleData['meta_data'][$parentTableKey] ?? $this->multipleData['meta_data']["parentTable_pk"];
   $childTableData = $this->multipleData['meta_data'][$childTableKey] ?? [];
   $backup = $this->multipleData['backup'] ?? false;
   $isCascade = $this->multipleData['cascade'] ?? false;
   // Check if parentTableData or parentTablePk is provided


   $this->data = [
       "relationship" => $relationship,
       "parentTable" => $parentTable,
       "childTable" => $childTable,
       "backup" => $backup,
       "cascade" => $isCascade
   ];
   for ($i = 0; $i < count($parentTableData); $i++) {
    $parentTableValue = $parentTableData[$i];
    $childTableValue = $childTableData[$i]; // Since childTableData is an array of arrays
    $this->data[$key] = $parentTableValue;
    $this->data[$childTableKey] =    $childTableValue;
    echo json_encode(["key" => $parentTableValue, "childTableValue" => $childTableValue]);
   $ids[] = $this->insertOne();}



   return $ids;
}




    public function getLastInsertedId()
    {
        return ["{$this->tableName}_id" => $this->lastInsertedId()];
    }

    public function getlastInsertedIds()
    {
        return $this->lastInsertedIds();
    }


}
?>
