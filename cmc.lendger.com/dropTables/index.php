<?php
require_once('../database/DeleteTables.php');

class DropTables extends DeleteTables
{

 function __construct()
 {
   header("Content-Type: application/json");
   if ($_SERVER["REQUEST_METHOD"] !== "DELETE") {
     echo json_encode(["error_message" => "Only the delete request is allowed"]);
     exit();
   }

   // Get the tables to delete from the URL query parameter
   $tablesToDelete = $_GET['tablesToDelete'] ?? '';

   // Decode the JSON data into an array
   $tablesToDeleteArray = json_decode(urldecode($tablesToDelete), true);

   // Pass the tables to delete to the parent class constructor
   $settings = ["tablesToDelete" => $tablesToDeleteArray];

   parent::__construct($settings);


   try {
     $this->dropTables();
     
     echo json_encode(["successful_message" => "All the tables entered were deleted"]);
   } catch (\Exception $e) {
     echo json_encode(["error_message" => $e->getMessage()]);
   }
 }
}

new DropTables();
?>
