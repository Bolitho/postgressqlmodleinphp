<?php require_once('../database/DatabaseFetcher.php');

class FilterRecord extends DatabaseFetcher
{
    private $year;
    private $month;
    private $order;
    private $conditions;
    private $columnName;

    public function __construct()
    {
        header("Content-Type: application/json");

        $this->year = trim($_GET['year']) ?? null;
        $this->month = trim($_GET['month']) ?? null;
        $this->order = trim($_GET['order']) ?? "ASC";
        $this->columnName = trim($_GET['column']) ?? null;

        if (
            $this->year === null ||
            $this->month === null ||
            $this->columnName === null
        ) {
            throw new Exception("Error: The year, month, or column name is required");
        }

        $this->conditions = [
            'AND' => [
                'year' => $this->year,
                'month' => $this->month
            ]
        ];

        parent::__construct($_GET['table']);

        try {
            $response = $this->find($this->conditions, $this->columnName, $this->order);
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }
}

new FilterRecord();
 ?>
