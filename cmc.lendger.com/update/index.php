<?php
require_once '../database/DatabaseUpdater.php';

class Update extends DatabaseUpdater
{
    private $isUpdateMany = false;

    public function __construct()
    {
        header("Content-Type: application/json");

        // Check if the request method is PUT
        if ($_SERVER['REQUEST_METHOD'] !== 'PUT') {
            http_response_code(405); // Method Not Allowed
            echo json_encode(['error' => 'Only PUT requests are allowed']);
            exit();
        }

        // Retrieve data from the request body
        $data = json_decode(file_get_contents('php://input'), true);
        if (isset($data["isUpdateMany"])) {
            $this->isUpdateMany = $data["isUpdateMany"];
        }

        // Check if the 'table' parameter is provided in the request body
        if (!isset($data['table'])) {
            http_response_code(400); // Bad Request
            echo json_encode(['error' => 'The \'table\' parameter is required']);
            exit();
        }

        // Extract data, and update the variable name to 'meta_data'
        $table = $data['table'];
        $conditions = $data["conditions"];
        $new_data = $data["meta_data"]; // Updated variable name

        // Add 'multiRowMultiValueUpdate' to the settings array
        $settings = [
            "table" => $table,
            "data" => $new_data,
            "conditions" => $conditions,
            "isManyToMany" => $this->isUpdateMany,
            "updateIsToDoneFromParent"=>$data["updateIsToDoneFromParent"]??false,
            "multiRowMultiValueUpdate" => $data["multiRowMultiValueUpdate"] ?? false // Set default to false
        ];

        parent::__construct($settings);

        try {

            if ($this->isUpdateMany) {
                $ids = $this->updateMany();
                echo json_encode(["success" => true,  "ids" => $ids]);
            } else {
                $id = $this->updateOne();
                echo json_encode(["success" => true, "id" => $id]);
            }
        } catch (\Exception $e) {
            echo json_encode(["error" => $e->getMessage()]);
        }
    }
}

// Create an instance of the Update class
new Update();
?>
