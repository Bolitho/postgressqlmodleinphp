<?php require_once("connection.php");

class APIHandler {
    private $databaseConnection;
    private $table = "ledgerEntries";

    public function __construct() {
        $this->databaseConnection = DatabaseConnection::getInstance();
        echo json_encode(["success" => "connected"]);
    }
    private function validatePostRequest()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            http_response_code(405); // Method Not Allowed
            echo json_encode(['error' => 'Only POST requests are allowed']);
            exit();
        }

        $data = json_decode(file_get_contents('php://input'), true);

        // Validate the required fields
       if (!isset($data['firstname']) || !isset($data['lastname']) || !isset($data['amount'])) {
            http_response_code(400); // Bad Request
            echo json_encode(['error' => 'Missing required fields']);
            exit();
        }

        return $data;
    }

    private function saveDataToLedgerBook($data)
  {
      // Extract the fields
      $firstname = $data['firstname'];
      $lastname = $data['lastname'];
      $amount = $data['amount'];

      // Save the data to the ledger book (example: append to a file)

      $entry = [
          'firstname' => $firstname,
          'lastname' => $lastname,
          'amount' => $amount
      ];

      try {
          $id = $this->databaseConnection->insertInfo($this->table, $entry);

          $response = [
              'id' => $id,
              'message' => 'Data received and saved successfully'
          ];
         echo json_encode($response);

      } catch (\Exception $e) {
          throw new \Exception('Failed to save data: ' . $e->getMessage());
          // Log the error or handle it in an appropriate way
      }
  }


private function handlePostRequest()
{
   header("Content-Type: application/json");

   $data = $this->validatePostRequest();
   //echo json_encode(["data"=>$data]);
   $this->saveDataToLedgerBook($data);
}



    private function requiredFieldsAreMissing($data)
  {
      return !isset($data['id']) ||
             empty($data['firstname']) ||
             empty($data['lastname']) ||
             !isset($data['amount']);
  }


  private function handleDeleteRequest()
 {
     header("Content-Type: application/json");

     if ($_SERVER["REQUEST_METHOD"] !== "DELETE") {
         http_response_code(405);
         echo json_encode(["error" => "Only the DELETE method is allowed"]);
         exit();
     }


     $id = trim($_GET['id']) ?? null;
     $conditions = ["AND"=>["id" => $id]];

     try {
         $this->databaseConnection->deleteOne($this->table, $conditions);
         echo json_encode(["message" => "The record was successfully deleted"]);
     } catch (Exception $e) {
         http_response_code(400);
         throw new Exception("delete_error", $e->getMessage());
     }
 }





    public function handleRequest() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->handlePostRequest();
        } elseif ( $_SERVER["REQUEST_METHOD"]==="DELETE") {
          $this->handleDeleteRequest();
        }

         else {
            http_response_code(405); // Method Not Allowed
            echo json_encode(['error' => 'Only GET and POST requests are allowed']);
        }
    }
}

// Create an instance of the HandlePostRequest class
$api = new APIHandler();
$api->handleRequest();
 ?>
