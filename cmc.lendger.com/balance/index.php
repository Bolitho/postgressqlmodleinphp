<?php
require_once('../database/Computions.php');

class Balance extends Computations
{
    private $tableName;
    private $data;

    public function __construct()
    {
        header("Content-Type: application/json");

        if ($_SERVER["REQUEST_METHOD"] !== "GET") {
            http_response_code(405);
            echo json_encode(["error_message" => "Only GET requests are allowed"]);
            exit();
        }

        parent::__construct([]);

        // Parse the JSON input data from the request body
        $inputData = file_get_contents("php://input");
        $this->data = json_decode($inputData, true);

        // Check if JSON decoding is successful
        if ($this->data === null && json_last_error() !== JSON_ERROR_NONE) {
            http_response_code(400);
            echo json_encode(["error_message" => "Invalid JSON data"]);
            exit();
        }

        if (empty($this->data["tableName"])&&!isset($this->data["operationIsDifferentTables"])) { // Check for missing table name
            http_response_code(400);
            echo json_encode(["error_message" => "The table name is missing"]);
            exit();
        }

        try {
            $this->tableName = $this->data["tableName"]??null;
            $response;
            if (isset($this->data["operationIsDifferentTables"]) && $this->data["operationIsDifferentTables"]) {
                $response = $this->balanceBetweenSumOfColumnsInDifferentTables($this->data);
            } else {
                $response = $this->balance($this->data);
            }
            echo json_encode(["response" => $response]);
        } catch (\PDOException $e) {
            // Database connection error
            http_response_code(500); // Internal Server Error
            echo json_encode(["error_message" => "Database connection error: " . $e->getMessage()]);
        } catch (\Exception $e) {
            // Other exceptions
            http_response_code(500); // Internal Server Error
            echo json_encode(["error_message" => $e->getMessage()]);
        }
    }
}

new Balance();
?>
